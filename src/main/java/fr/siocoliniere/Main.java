package fr.siocoliniere;

import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.view.WelcomeForm;

public class Main {
	public static void main(String[] args) throws DALException {
		System.out.println("Lancement de l'application");

		WelcomeForm welcomeView = new WelcomeForm();
		welcomeView.setVisible(true);
	}
}
