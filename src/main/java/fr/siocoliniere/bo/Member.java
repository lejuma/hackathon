package fr.siocoliniere.bo;

/**
 * Classe métier Member
 * Permet l'instanciation d'un membre
 */
public class Member {
	private int id;
	private String lastname;
	private String firstname;
	private String mail;

	/**
	 * Constructeur
	 * @param lastname nom du membre
	 * @param firstname prénom du membre
	 * @param mail adresse e-mail du membre
	 * @param id id du membre
	 */
	public Member(int id, String firstname, String lastname, String mail) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.mail = mail;
	}
	/**
	 * Constructeur
	 * @param lastname nom du membre
	 * @param firstname prénom du membre
	 * @param mail adresse e-mail du membre
	 */
	public Member(String firstname, String lastname, String mail) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.mail = mail;
	}

	public int getId() {
		return id;
	}
	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public String getMail() {
		return mail;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public String toString() {
		return  lastname + ' ' + firstname ;
	}
}
