package fr.siocoliniere.bo;

public class Phase {
	private int id;
	private String name;

	/**
	 * Constructeur
	 * @param unNom nom de la phase
	 */
	public Phase(String unNom) {
		this.id = 1;
		this.name = unNom;
	}


	/**
	 * Constructeur
	 * @param unId id de la phase
	 * @param unNom nom de la phase
	 */
	public Phase(int unId, String unNom) {
		this.id = unId;
		this.name = unNom;
	}


	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}

	public void setId(int unId) {
		this.id = unId;
	}
	public void setName(String unNom) {
		this.name = unNom;
	}


	@Override
	public String toString() {
		return this.name;
	}
}

