package fr.siocoliniere.bo;

import java.util.ArrayList;
import java.util.List;

public class TypeInscription {
	private int id;
	private String name;

	/**
	 * Constructeur
	 * @param unNom nom du type d'incription
	 */
	public TypeInscription(String unNom) {
		this.id = 0;
		this.name = unNom;
	}


	/**
	 * Constructeur
	 * @param unId id du type d'incription
	 * @param unNom nom du type d'incription
	 */
	public TypeInscription(int unId, String unNom) {
		this.id = unId;
		this.name = unNom;
	}

	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}

	public void setId(int unId) {
		this.id = unId;
	}
	public void setName(String unNom) {
		this.name = unNom;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
