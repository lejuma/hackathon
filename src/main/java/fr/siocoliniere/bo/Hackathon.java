package fr.siocoliniere.bo;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe métier Hackathon
 * Permet l'instanciation d'un hackathon
 */
public class Hackathon {
	private int id;					//id de l'hackathon
	private String name;			//nom du hackathon
	private String topic;			//sujet du hackathon
	private String description;		//description du hackathon
	private Date periodBegin;		//début du hackathon
	private Date periodEnd;		//fin du hackathon
	private int idLieu;				//id du lieu du hackathon
	private int typeInscriptionId; 	//id du type d'inscription du hackathon
	private int idPhase; 		//id de la phase du hackathon

	private List<Member> juryMembers;	// liste des membres du jury du hackathon
	private List<Member> animMembers;	// liste des membres des animateurs du hackathon
    private List<Member> expertMembers; // liste des membres des experts du hackathon
    private Member tmpMember; // membre venant d'être créé dans CreateNewForm devant être envoyé dans ManagementForm

	/**
	 * Constructeur
	 * @param unNom nom du hackathon
	 * @param unTopic sujet du hackathon
	 * @param uneDescription description du hackathon
	 * @param unePeriodBegin début du hackathon
	 * @param unePeriodEnd fin du hackathon
	 * @param unIdLieu id du lieu du hackathon
	 * @param unTypeInscriptionId id du type d'inscription du hackathon
	 * @param unIdPhase id de la phase du hackathon
	 */
	public Hackathon(String unNom,String unTopic, String uneDescription, Date unePeriodBegin, Date unePeriodEnd, int unIdLieu, int unTypeInscriptionId, int unIdPhase) {
		this.id = 0;
		this.name = unNom;
		this.topic = unTopic;
		this.description = uneDescription;
		this.periodBegin = unePeriodBegin;
		this.periodEnd = unePeriodEnd;
		this.idLieu = unIdLieu;
		this.typeInscriptionId = unTypeInscriptionId;
		this.idPhase = unIdPhase;

        this.juryMembers = new ArrayList<>();
        this.expertMembers = new ArrayList<>();
		this.animMembers = new ArrayList<>();

        this.tmpMember = null;
    }

	/**
	 * Constructeur
	 * @param unId id du hackathon
	 * @param unTopic sujet du hackathon
	 * @param uneDescription description du hackathon
	 * @param unePeriodBegin début du hackathon
	 * @param unePeriodEnd fin du hackathon
	 * @param unIdLieu id du lieu du hackathon
	 * @param unTypeInscriptionId id du type d'inscription du hackathon
	 * @param unIdPhase id de la phase du hackathon
	 */
	public Hackathon(int unId, String unNom,String unTopic, String uneDescription, Date unePeriodBegin, Date unePeriodEnd, int unIdLieu, int unTypeInscriptionId, int unIdPhase) {
		this(unNom, unTopic, uneDescription, unePeriodBegin, unePeriodEnd, unIdLieu, unTypeInscriptionId, unIdPhase);
		this.id = unId;
	}

	public int getId() {
		return id;
	}
	public String getName()  {
		return name;
	}
	public String getTopic() {
		return topic;
	}
	public String getDescription() {
		return description;
	}
	public Date getPeriodBegin() {
		return periodBegin;
	}
	public Date getPeriodEnd() {
		return periodEnd;
	}
	public int getTypeInscriptionId() {
		return typeInscriptionId;
	}
	public int getIdLieu() {
		return idLieu;
	}
	public int getIdPhase() {
		return idPhase;
	}

	public List<Member> getJuryMembers() {
		return juryMembers;
	}
	public List<Member> getAnimMembers() {
		return animMembers;
	}
	public List<Member> getExpertMembers() {
		return expertMembers;
	}
	public Member getTmpMember() {
		return tmpMember;
	}

    public void setId(int unId) {
        this.id = unId;
    }
    public void setName(String unNom) {
        this.name = unNom;
    }
    public void setTopic(String unTopic) {
        this.topic = unTopic;
    }
    public void setDescription(String uneDescription) {
        this.description = uneDescription;
    }
    public void setJuryMembers(List<Member> lesJuryMembers) {this.juryMembers = lesJuryMembers;}
    public void setAnimMembers(List<Member> lesAnimMembers) {this.animMembers = lesAnimMembers;}
    public void setExpertMembers(List<Member> lesExpertMembers) {this.expertMembers = lesExpertMembers;}
    public void setTmpMember(Member unMembre) {this.tmpMember = unMembre;}

	public void setPeriodBegin(Date unePeriodBegin) {
		this.periodBegin = unePeriodBegin;
	}
	public void setPeriodEnd(Date unePeriodEnd) {
		this.periodEnd = unePeriodEnd;
	}
	public void setTypeInscriptionId(int unTypeInscriptionId) {
		this.typeInscriptionId = unTypeInscriptionId;
	}
	public void setIdLieu(int unIdLieu) {
		this.idLieu = unIdLieu;
	}
	public void setIdPhase(int unIdPhase) {
		this.idPhase = unIdPhase;
	}

    /**
     * Permet l'ajout d'un membre au jury du hackathon
     * @param member nouveau membre
     */
    public void addJuryMember(Member member){
        if (member != null){
            juryMembers.add(member);
        }
    }

    /**
     * Permet l'ajout d'un membre au groupe d'experts du hackathon
     * @param member nouveau membre
     */
    public void addExpertMember(Member member) {
        if (member != null) {
            expertMembers.add(member);
        }
    }

    /**
     * Permet l'ajout d'un membre au groupe d'animateurs du hackathon
     * @param member nouveau membre
     */
    public void addAnimMember(Member member){
        if (member != null) {
            animMembers.add(member);
        }
    }

	@Override
	public String toString() {
		return this.name + " — " + this.topic;
	}

	/**
	 * Permet de vider la liste de membres composant le jury
	 */
	public void clearJuryMember() {
		this.juryMembers.clear();
	}

    /**
     * Permet de vider la liste de membres animateurs
     */
    public void clearAnimMember() {
        this.animMembers.clear();
    }

    /**
     * Permet de vider la liste de membres experts
     */
    public void clearExpertMember() {
        this.expertMembers.clear();
    }
 }
