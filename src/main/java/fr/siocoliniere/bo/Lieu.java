package fr.siocoliniere.bo;

public class Lieu {
	private int id;
	private String nom;
	private String ville;

	/**
	 * Constructeur
	 * @param unNom nom du lieu
	 * @param uneVille nom de la ville
	 */
	public Lieu(String unNom, String uneVille) {
		this.id = 0;
		this.nom = unNom;
		this.ville = uneVille;
	}


	/**
	 * Constructeur
	 * @param unId id du lieu
	 * @param unNom nom du lieu
	 * @param uneVille nom de la ville
	 */
	public Lieu(int unId, String unNom, String uneVille) {
		this.id = unId;
		this.nom = unNom;
		this.ville = uneVille;
	}


	public int getId() {
		return id;
	}
	public String getNom() {
		return nom;
	}
	public String getVille() {
		return ville;
	}

	public void setId(int unId) {
		this.id = unId;
	}
	public void setNom(String unNom) {
		this.nom = unNom;
	}
	public void setVille(String uneVille) {
		this.ville = uneVille;
	}



	@Override
	public String toString() {
		return this.nom + ", " + this.ville;
	}
}

