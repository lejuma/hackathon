package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Lieu;
import fr.siocoliniere.bo.TypeInscription;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe DAOHackathon
 * Implémente la classe abstraite DAO
 * BD : Table TypeInscription
 */
public class DAOTypeInscription extends DAO<TypeInscription> {
	/**
	 * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
	 */
	private static DAOTypeInscription instanceDAOTypeInscription;
	private TypeInscription obj;

	/**
	 * Pattern Singleton
	 * @return DAOHackathon
	 */
	public static synchronized DAOTypeInscription getInstance() {
		if (instanceDAOTypeInscription == null) {
			instanceDAOTypeInscription = new DAOTypeInscription();
		}
		return instanceDAOTypeInscription;
	}



	/**
	 * REQUETES
	 */
	private static final String sqlSelectAll = "select * from typeinscription order by id";
	private static final String sqlSelectIdByName = "select id from typeinscription where name = ?";
	private static final String sqlSelectOneById = "select * from typeinscription where id = ?";
	private static final String sqlUpdateOne = "update typeinscription set name = ? where id = ?";
	private static final String sqlInsertOne = "insert into typeinscription(name) values (?)";
	private static final String sqlDeleteOneById = "delete from typeinscription where id = ?";



	/**
	 * Permet de récupérer tous les types d'inscription stockés dans la table TypeInscription
	 * @return la liste des types d'inscription des hackathons
	 * @throws DALException
	 */
	@Override
	public List<TypeInscription> getAll() throws DALException {

		List<TypeInscription> typesList = new ArrayList<>();

		try {
			Connection cnx = JdbcTools.getConnection();
			Statement rqt = cnx.createStatement();
			ResultSet rs = rqt.executeQuery(sqlSelectAll);

			while (rs.next()) {
				//instanciation du hackathon
				TypeInscription unType = new TypeInscription(rs.getInt("id"),rs.getString("name"));
				typesList.add(unType);
			}
			rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return typesList;
	}


	/**
	 * Permet de récupérer l'id d'un TypeInscription connaissant le nom (contrainte d'unicité sur le nom)
	 * @param name : nom du TypeInscription recherché
	 * @return id du TypeInscription
	 * @throws DALException
	 */
	public Integer getIdByName(String name) throws DALException {

		Integer idLieu = null;
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectIdByName);
			rqt.setString(1, name);
			ResultSet rs = rqt.executeQuery();
			while (rs.next()) {
				//instanciation du hackathon
				idLieu = rs.getInt("id");
			}
			rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return idLieu;
	}


	/**
	 * Permet de récupérer un hackathon à partir de son id
	 * @param id du type d'inscription
	 * @return le type d'inscription dont l'id est transmis en paramètre
	 * @throws DALException
	 */
	@Override
	public TypeInscription getOneById(int id) throws DALException {
		TypeInscription unLieu = null;

		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectOneById);
			rqt.setInt(1, id);

			ResultSet rs = rqt.executeQuery();

			while (rs.next()) {
				unLieu = new TypeInscription(rs.getInt("id"),rs.getString("name"));
			}
			rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return unLieu;
	}


	/**
	 * Permet l'insertion d'un nouveau TypeInscription
	 * @param obj : hackathon à inserer
	 * @throws DALException
	 */
	public void saveTypeInscription(TypeInscription obj) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlInsertOne);
			rqt.setString(1, obj.getName());
			rqt.executeUpdate();
			rqt.close();

			//mise à jour de l'id du hackathon nouvellement créé - le champ name est UNIQUE
			obj.setId(this.getIdByName(obj.getName()));

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
	}


	/**
	 * Permet de mettre à jour uniquement les settings du hackathon dans la base de données
	 * @param obj : hackathon à mettre à jour
	 * @throws DALException
	 */
	public void updateSettings(TypeInscription obj) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlUpdateOne);
			rqt.setString(1, obj.getName());
			rqt.setInt(2, obj.getId());
			rqt.executeUpdate();
			rqt.close();

		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}


	public void deleteTypeInscriptionById(int idlieu) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlDeleteOneById);
			rqt.setInt(1, idlieu);
			rqt.executeUpdate();
			rqt.close();
		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}


	@Override
	public void save(TypeInscription obj) throws DALException {
		saveTypeInscription(obj);
	}

	@Override
	public void update(TypeInscription obj) throws DALException {
		updateSettings(obj);
	}
}
