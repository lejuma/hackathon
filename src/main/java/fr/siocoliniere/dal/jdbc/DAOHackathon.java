package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe DAOHackathon
 * Implémente la classe abstraite DAO
 * BD : Table Hackathon
 */
public class DAOHackathon extends DAO<Hackathon> {

	/**
	 * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
	 */
	private static DAOHackathon instanceDAOHackathon;
	private Hackathon obj;

	/**
	 * Pattern Singleton
	 * @return DAOHackathon
	 */
	public static synchronized DAOHackathon getInstance() {
		if (instanceDAOHackathon == null) {
			instanceDAOHackathon = new DAOHackathon();
		}
		return instanceDAOHackathon;
	}


	/**
	 * REQUETES
	 */
	private static final String sqlSelectAll = "select * from hackathon h order by h.name";
	private static final String sqlSelectIdByName= "select h.id from hackathon h where h.name = ? order by h.name";
	private static final String sqlSelectOneById = "select * from hackathon h where h.id = ?";
	private static final String sqlUpdateOne = "update hackathon set name = ?, topic = ?, description = ?, periodBegin = ?, periodEnd = ?, idlieu = ?, typeinscriptionid = ?, idphase = ? where id = ?";
	private static final String sqlInsertOne = "insert into hackathon(name, topic, description, periodBegin, periodEnd, idlieu, typeinscriptionid, idphase) values (?,?,?,?,?,?,?,?)";

	private static final String sqlInsertJuryMember = "insert into participation(hackathonid,memberid,roleid) values (?,?,1)";
	private static final String sqlInsertAnimMember = "insert into participation(hackathonid,memberid,roleid) values (?,?,3)";
	private static final String sqlInsertExpertMember = "insert into participation(hackathonid,memberid,roleid) values (?,?,4)";
	private static final String sqlDeleteAllJuryMember = "delete from participation where hackathonid = ? and roleid = 1";
	private static final String sqlDeleteAllAnimMember = "delete from participation where hackathonid = ? and roleid = 3";
	private static final String sqlDeleteAllExpertMember = "delete from participation where hackathonid = ? and roleid = 4";
    private static final String sqlDeleteParticipationByIdHackathon = "delete from participation where hackathonid = ?";

    private static final String sqlDeleteHackathonById = "delete from hackathon where id = ?";



	/**
	 * Permet de supprimer la composition du jury du hackathon
	 * @param hackid : id du hackathon
	 * @throws DALException
	 */
	public void deleteAllParticipationJuryOfHackathon(int hackid) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlDeleteAllJuryMember);
			rqt.setInt(1, hackid);
			rqt.executeUpdate();
			rqt.close();
		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}

	/**
	 * Permet de supprimer les animateurs du hackathon
	 * @param hackid : id du hackathon
	 * @throws DALException
	 */
    public void deleteAllParticipationAnimOfHackathon(int hackid) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlDeleteAllAnimMember);
            rqt.setInt(1, hackid);
            rqt.executeUpdate();
            rqt.close();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(),e);
        }
    }

	/**
	 * Permet de supprimer les experts du hackathon
	 * @param hackid : id du hackathon
	 * @throws DALException
	 */
    public void deleteAllParticipationExpertOfHackathon(int hackid) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlDeleteAllExpertMember);
            rqt.setInt(1, hackid);
            rqt.executeUpdate();
            rqt.close();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(),e);
        }
    }

	public void deleteParticipationByIdHackathon(int hackid) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlDeleteParticipationByIdHackathon);
			rqt.setInt(1, hackid);
			rqt.executeUpdate();
			rqt.close();
		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}

	public void deleteHackathonById(int hackid) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlDeleteHackathonById);
			rqt.setInt(1, hackid);
			rqt.executeUpdate();
			rqt.close();
		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}


	/**
	 * Permet de récupérer tous les hackathons stockés dans la table Hackathon
	 * @return la liste des hackathons
	 * @throws DALException
	 */
	@Override
	public List<Hackathon> getAll() throws DALException {

		List<Hackathon> hackathonList = new ArrayList<>();

        List<Member> juryList;
        List<Member> animList;
        List<Member> expertList;

		try {
			Connection cnx = JdbcTools.getConnection();
			Statement rqt = cnx.createStatement();
			ResultSet rs = rqt.executeQuery(sqlSelectAll);

			while (rs.next()) {
				//instanciation du hackathon
				Hackathon hacka = new Hackathon(rs.getInt("id"),rs.getString("name"),rs.getString("topic"), rs.getString("description"), rs.getDate("periodBegin"), rs.getDate("periodEnd"), rs.getInt("idlieu"), rs.getInt("typeinscriptionid"), rs.getInt("idphase"));

                //récupération des membres par appel au DAO gérant les membres
                juryList = DAOMember.getInstance().getAllJuryByIdHackathon(hacka.getId());
                animList = DAOMember.getInstance().getAllAnimByIdHackathon(hacka.getId());
                expertList = DAOMember.getInstance().getAllExpertByIdHackathon(hacka.getId());
                hacka.setJuryMembers(juryList);
                hacka.setAnimMembers(animList);
                hacka.setExpertMembers(expertList);
                hackathonList.add(hacka);
            }
            rqt.close();
		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return hackathonList;
	}


	/**
	 * Permet de récupérer l'id d'un hackathon connaissant le nom (contrainte d'unicité sur le nom)
	 * @param name : nom du hackathon recherché
	 * @return id du hackathon
	 * @throws DALException
	 */
	public Integer getIdByName(String name) throws DALException {

		Integer hackid = null;
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectIdByName);
			rqt.setString(1,name);
			ResultSet rs = rqt.executeQuery();
			while (rs.next()) {
				//instanciation du hackathon
				hackid=rs.getInt("id");
			}
			rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return hackid ;
	}


	/**
	 * Permet de récupérer un hackathon à partir de son id
	 * @param id du hackathon
	 * @return le hackathon dont l'id est transmis en paramètre
	 * @throws DALException
	 */
	@Override
	public Hackathon getOneById(int id) throws DALException {
		Hackathon hackat = null;

		List<Member> juryList;
		List<Member> animList;
		List<Member> expeList;
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectOneById);
			rqt.setInt(1, id);

			ResultSet rs = rqt.executeQuery();

			while (rs.next()) {
				hackat = new Hackathon(rs.getInt("id"),rs.getString("name"),rs.getString("topic"), rs.getString("description"), rs.getDate("periodBegin"), rs.getDate("periodEnd"), rs.getInt("idLieu"), rs.getInt("typeinscriptionId"), rs.getInt("idphase"));

				//récupération des membres par appel au DAO gérant les membres
				juryList = DAOMember.getInstance().getAllJuryByIdHackathon(rs.getInt("id"));
				animList = DAOMember.getInstance().getAllAnimByIdHackathon(rs.getInt("id"));
				expeList = DAOMember.getInstance().getAllExpertByIdHackathon(rs.getInt("id"));
				hackat.setJuryMembers(juryList);
				hackat.setAnimMembers(animList);
				hackat.setExpertMembers(expeList);
			}
			rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return hackat;
	}



	/**
	 * Permet l'insertion d'un nouvel hackathon
	 * @param obj : hackathon à inserer
	 * @throws DALException
	 */
	public void saveHackathon(Hackathon obj) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlInsertOne);
			rqt.setString(1, obj.getName());
			rqt.setString(2, obj.getTopic());
			rqt.setString(3, obj.getDescription());
			rqt.setDate(4, obj.getPeriodBegin());
			rqt.setDate(5, obj.getPeriodEnd());
			rqt.setInt(6, obj.getIdLieu());
			rqt.setInt(7,obj.getTypeInscriptionId());
			rqt.setInt(8,obj.getIdPhase());
			rqt.executeUpdate();
			rqt.close();

			//mise à jour de l'id du hackathon nouvellement créé - le champ name est UNIQUE
			obj.setId(this.getIdByName(obj.getName()));

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
	}


	/**
	 * Permet de mettre à jour la composition du jury du hackathon
	 * @param hackid : hackathon auquel le jury participe
	 * @throws DALException
	 */
	public void saveParticipationRoleJury(int hackid, int idJury) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlInsertJuryMember);
			rqt.setInt(1, hackid);
			rqt.setInt(2, idJury);
			rqt.executeUpdate();
			rqt.close();

		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}
	/**
	 * Permet de mettre à jour la composition du groupe d'animateurs du hackathon
	 * @param hackid : hackathon auquel l'animateur participe
	 * @throws DALException
	 */
    public void saveParticipationRoleAnim(int hackid, int idAnim) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertAnimMember);
            rqt.setInt(1, hackid);
            rqt.setInt(2, idAnim);
            rqt.executeUpdate();
            rqt.close();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(),e);
        }
    }

	/**
	 * Permet de mettre à jour la composition du groupe d'expert du hackathon
	 * @param hackid : hackathon auquel l'expert participe
	 * @throws DALException
	 */
    public void saveParticipationRoleExpert(int hackid, int idExpert) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertExpertMember);
            rqt.setInt(1, hackid);
            rqt.setInt(2, idExpert);
            rqt.executeUpdate();
            rqt.close();

		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}

	/**
	 * Permet de mettre à jour uniquement les settings du hackathon dans la base de données
	 * @param obj : hackathon à mettre à jour
	 * @throws DALException
	 */
	public void updateSettings(Hackathon obj) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlUpdateOne);
			rqt.setString(1, obj.getName());
			rqt.setString(2, obj.getTopic());
			rqt.setString(3, obj.getDescription());
			rqt.setDate(4, obj.getPeriodBegin());
			rqt.setDate(5, obj.getPeriodEnd());
			rqt.setInt(6, obj.getIdLieu());
			rqt.setInt(7, obj.getTypeInscriptionId());
			rqt.setInt(8, obj.getIdPhase());
			rqt.setInt(9, obj.getId());
			rqt.executeUpdate();
			rqt.close();

		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}


	@Override
	public void save(Hackathon obj) throws DALException {
		saveHackathon(obj);
	}

	@Override
	public void update(Hackathon obj) throws DALException {
		updateSettings(obj);
	}
}
