package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe DAOMember
 * Implémente la classe abstraite DAO
 * BD : Table Member
 */
public class DAOMember extends DAO<Member> {

	/**
	 * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
	 */
	private static DAOMember instanceDAOMember;
	private Member obj;

	/**
	 * Pattern Singleton
	 * @return DAOMember
	 */
	public static synchronized DAOMember getInstance() {
		if (instanceDAOMember == null) {
			instanceDAOMember = new DAOMember();
		}
		return instanceDAOMember;
	}

    /**
     * REQUETES
     */
    private static final String sqlSelectAll = "select id, firstname, lastname, mail from member order by lastname, firstname";
    private static final String sqlSelectMemberIdByName= "select id from member where firstname = ? and lastname = ? and mail = ?";
    private static final String sqlSelectOneById = "select id, firstname, lastname, mail from member where id = ?";
    private static final String sqlSelectAllParticipationJuryByIdHackathon = "select m.id, m.lastname, m.firstname, m.mail from member m inner join participation p on m.id = p.memberid where p.hackathonid = ? and p.roleid = 1";
    private static final String sqlSelectAllParticipationAnimByIdHackathon = "select m.id, m.lastname, m.firstname, m.mail from member m inner join participation p on m.id = p.memberid where p.hackathonid = ? and p.roleid = 3";
    private static final String sqlSelectAllParticipationExpertByIdHackathon = "select m.id, m.lastname, m.firstname, m.mail from member m inner join participation p on m.id = p.memberid where p.hackathonid = ? and p.roleid = 4";
    private static final String sqlSelectAllJuryMember = "select distinct m.id, m.lastname, m.firstname, m.mail from member m inner join participation p on m.id = p.memberid where p.roleid = 1";
    private static final String sqlSelectAllAnimMember = "select distinct m.id, m.lastname, m.firstname, m.mail from member m inner join participation p on m.id = p.memberid where p.roleid = 3";
    private static final String sqlSelectAllExpertMember = "select distinct m.id, m.lastname, m.firstname, m.mail from member m inner join participation p on m.id = p.memberid where p.roleid = 4";
    private static final String sqlSelectOnlyAnimMemberFromOtherHackathon = "select distinct m.id, m.lastname, m.firstname, m.mail from member m inner join participation p on m.id = p.memberid where p.roleid = 3 and p.memberid not in (select memberid from participation where hackathonid = ?)";
    private static final String sqlSelectOnlyExpertMemberFromOtherHackathon = "select distinct m.id, m.lastname, m.firstname, m.mail from member m inner join participation p on m.id = p.memberid where p.roleid = 4 and p.memberid not in (select memberid from participation where hackathonid = ?)";
    private static final String sqlSelectJuryMemberFromOtherHackathonOnly = "select distinct m.id, m.lastname, m.firstname, m.mail from member m inner join participation p on m.id = p.memberid where p.roleid = 1 and m.id not in (select distinct memberid from participation where hackathonid = ?)";
    private static final String sqlSelectNonRoleMemberOnly = "select distinct m.id, m.lastname, m.firstname, m.mail from member m where m.id not in (select distinct m.id from member m inner join participation p on m.id = p.memberid where roleid = ?)";
    private static final String sqlUpdateMember = "update member set firstname = ?, lastname = ?, mail = ? where id = ?";
    private static final String sqlInsertNewMember = "insert into member (firstname, lastname, mail) values (?, ?, ?)";


    /**
     * Permet de récupérer l'ensemble des membres stockés dans la table membre
     * @return
     * @throws DALException
     */
    @Override
    public List<Member> getAll() throws DALException {
        List<Member> memberList = new ArrayList<>();
        try {
            Connection cnx = JdbcTools.getConnection();
            Statement rqt = cnx.createStatement();
            ResultSet rs = rqt.executeQuery(sqlSelectAll);

            while (rs.next()) {
                //instanciation du membre
                Member member = new Member(rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("mail"));

                //récupération des membres par appel au DAO gérant les membres
                memberList.add(member);
            }
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }

        return memberList;
    }


	/**
	 * Permet de récupérer l'ensemble des membres ayant participé à un hackathon avec le rôle "jury" (roleid = 1)
	 * @return liste de membres
	 * @throws DALException
	 */
	public List<Member> getAllJuryMember() throws DALException {

		List<Member> memberList = new ArrayList<>();
		try {
			Connection cnx = JdbcTools.getConnection();
			Statement rqt = cnx.createStatement();
			ResultSet rs = rqt.executeQuery(sqlSelectAllJuryMember);

            while (rs.next()) {
                Member member = new Member(rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("mail"));
                memberList.add(member);
            }
            rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(), e);
		}
		return memberList;
	}

	public List<Member> getAllAnimMember() throws DALException {

		List<Member> memberList = new ArrayList<>();
		try {
			Connection cnx = JdbcTools.getConnection();
			Statement rqt = cnx.createStatement();
			ResultSet rs = rqt.executeQuery(sqlSelectAllAnimMember);

            while (rs.next()) {
                Member member = new Member(rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("mail"));
                memberList.add(member);
            }
            rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(), e);
		}
		return memberList;
	}

    public List<Member> getAllExpertMember() throws DALException {

        List<Member> memberList = new ArrayList<>();
        try {
            Connection cnx = JdbcTools.getConnection();
            Statement rqt = cnx.createStatement();
            ResultSet rs = rqt.executeQuery(sqlSelectAllExpertMember);

            while (rs.next()) {
                Member member = new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname"), rs.getString("mail"));
                memberList.add(member);
            }
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(), e);
        }
        return memberList;
    }

    public List<Member> getOnlyAnimFromOtherHackathon(int idhack) throws DALException {

		List<Member> memberList = new ArrayList<>();
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectOnlyAnimMemberFromOtherHackathon);
			rqt.setInt(1,idhack);
			ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                Member member = new Member(rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("mail"));
                memberList.add(member);
            }
            rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(), e);
		}
		return memberList;
	}

    public List<Member> getOnlyExpertFromOtherHackathon(int idhack) throws DALException {

        List<Member> memberList = new ArrayList<>();
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectOnlyExpertMemberFromOtherHackathon);
            rqt.setInt(1,idhack);
            ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                Member member = new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname"), rs.getString("mail"));
                memberList.add(member);
            }
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(), e);
        }
        return memberList;
    }


	 /**
     * Permet de récupérer l'ensemble des membres ayant participé à un hackathon avec le rôle "jury" (roleid = 1) ne faisant pas parti du hackaton passé en param
     * @return liste de membres
     * @throws DALException
     */

     public List<Member> getNonRoleMembersOnly(int idrole) throws DALException {
         List<Member> memberList = new ArrayList<>();
         try {
             Connection cnx = JdbcTools.getConnection();
             PreparedStatement rqt = cnx.prepareStatement(sqlSelectNonRoleMemberOnly);
             rqt.setInt(1, idrole);
             ResultSet rs = rqt.executeQuery();

             while (rs.next()) {
                 Member member = new Member(rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("mail"));
                 memberList.add(member);
             }
             rqt.close();

         } catch (SQLException e) {
             //Exception personnalisée
             throw new DALException(e.getMessage(), e);
         }
         return memberList;
     }

    public List<Member> getAllJuryMemberFromOtherHackathonsOnly(int idhack) throws DALException {

		List<Member> memberList = new ArrayList<>();
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectJuryMemberFromOtherHackathonOnly);
			rqt.setInt(1, idhack);
			ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                Member member = new Member(rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("mail"));
                memberList.add(member);
            }
            rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(), e);
		}
		return memberList;
	}

	/**
	 * Permet de récupérer l'ensemble des membres ayant participé au hackathon dont l'id est passé en paramètre, avec le rôle "jury" (roleid = 1)
	 * @param hackid
	 * @return liste de membres
	 * @throws DALException
	 */
	public List<Member> getAllJuryByIdHackathon(int hackid) throws DALException {

		List<Member> juries = new ArrayList<>();

		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllParticipationJuryByIdHackathon);
			rqt.setInt(1, hackid);
			ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                juries.add(new Member(rs.getInt("id"),rs.getString("firstname"),rs.getString("lastname"), rs.getString("mail")));
            }
            rqt.close();

		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
		return juries;
	}

	public List<Member> getAllAnimByIdHackathon(int hackid) throws DALException {

		List<Member> animators = new ArrayList<>();

		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllParticipationAnimByIdHackathon);
			rqt.setInt(1, hackid);
			ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                animators.add(new Member(rs.getInt("id"),rs.getString("firstname"),rs.getString("lastname"), rs.getString("mail")));
            }
            rqt.close();

		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
		return animators;
	}

    public List<Member> getAllExpertByIdHackathon(int hackid) throws DALException {

        List<Member> experts = new ArrayList<>();

        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllParticipationExpertByIdHackathon);
            rqt.setInt(1, hackid);
            ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                experts.add(new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname"), rs.getString("mail")));
            }
            rqt.close();

        } catch (SQLException e) {
            throw new DALException(e.getMessage(),e);
        }
        return experts;
    }

    /**
     * Permet de récupérer le membre dont l'identifiant est passé en paramètre
     * @param id
     * @return un membre
     * @throws DALException
     */
    @Override
    public Member getOneById(int id) throws DALException {
        Member member=null;
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectOneById);
            rqt.setInt(1, id);
            ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                member = new Member(rs.getInt("id"),rs.getString("firstname"),rs.getString("lastname"), rs.getString("mail"));
            }
            rqt.close();

		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
		return member;
    }


    public void saveMember(Member member) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsertNewMember);
            rqt.setString(1, member.getFirstname());
            rqt.setString(2, member.getLastname());
            rqt.setString(3, member.getMail());
            rqt.execute();
            rqt.close();
        } catch (SQLException e) {
            throw new DALException(e.getMessage(),e);
        }
    }
    public Integer getIdByFirstLastName(String firstname, String lastname, String mail) throws DALException {
        Integer hackid = null;
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectMemberIdByName);
            rqt.setString(1,firstname);
            rqt.setString(2,lastname);
            rqt.setString(3,mail);
            ResultSet rs = rqt.executeQuery();
            while (rs.next()) {
                //instanciation du hackathon
                hackid=rs.getInt("id");
            }
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }
        return hackid ;
    }
    @Override
    public void save(Member obj) throws DALException {
        saveMember(obj);
    }

	@Override
	public void update(Member obj) throws DALException {
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlUpdateMember);
            rqt.setString(1,obj.getFirstname());
            rqt.setString(2,obj.getLastname());
            rqt.setString(3,obj.getMail());
            rqt.setInt(4,obj.getId());
            rqt.executeUpdate();
            rqt.close();

        } catch (SQLException e) {
            //Exception personnalisée
            throw new DALException(e.getMessage(),e);
        }
	}
}
