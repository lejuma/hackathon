package fr.siocoliniere.dal.jdbc;


import fr.siocoliniere.bo.Lieu;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe DAOHackathon
 * Implémente la classe abstraite DAO
 * BD : Table Lieu
 */
public class DAOLieu extends DAO<Lieu> {
	/**
	 * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
	 */
	private static DAOLieu instanceDAOLieu;
	private Lieu obj;

	/**
	 * Pattern Singleton
	 * @return DAOHackathon
	 */
	public static synchronized DAOLieu getInstance() {
		if (instanceDAOLieu == null) {
			instanceDAOLieu = new DAOLieu();
		}
		return instanceDAOLieu;
	}



	/**
	 * REQUETES
	 */

	private static final String sqlSelectAll = "select * from lieu l order by l.ville, l.nom";
	private static final String sqlSelectIdByName = "select id, ville from lieu where nom = ? and ville = ?";
	private static final String sqlSelectOneById = "select nom, ville from lieu where id = ?";
	private static final String sqlUpdateOne = "update lieu set nom = ?, ville = ? where id = ?";
	private static final String sqlInsertOne = "insert into lieu(nom, ville) values (?,?)";
	private static final String sqlDeleteOneById = "delete from lieu where id = ?";



	/**
	 * Permet de récupérer tous les lieux stockés dans la table Lieu
	 * @return la liste des lieux des hackathons
	 * @throws DALException
	 */
	@Override
	public List<Lieu> getAll() throws DALException {

		List<Lieu> lieuList = new ArrayList<>();

		try {
			Connection cnx = JdbcTools.getConnection();
			Statement rqt = cnx.createStatement();
			ResultSet rs = rqt.executeQuery(sqlSelectAll);

			while (rs.next()) {
				//instanciation du hackathon
				Lieu unLieu = new Lieu(rs.getInt("id"), rs.getString("nom"), rs.getString("ville"));
				lieuList.add(unLieu);
			}
			rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return lieuList;
	}


	/**
	 * Permet de récupérer l'id d'un lieu connaissant le nom (contrainte d'unicité sur le nom)
	 * @param nom : nom du lieu recherché
	 * @return id du lieu
	 * @throws DALException
	 */
	public Integer getIdByName(String nom, String ville) throws DALException {

		Integer idLieu = null;
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectIdByName);
			rqt.setString(1, nom);
			rqt.setString(2, ville);
			ResultSet rs = rqt.executeQuery();
			while (rs.next()) {
				//instanciation du hackathon
				idLieu = rs.getInt("id");
			}
			rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return idLieu;
	}


	/**
	 * Permet de récupérer un hackathon à partir de son id
	 * @param id du hackathon
	 * @return le hackathon dont l'id est transmis en paramètre
	 * @throws DALException
	 */
	@Override
	public Lieu getOneById(int id) throws DALException {
		Lieu unLieu = null;

		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectOneById);
			rqt.setInt(1, id);

			ResultSet rs = rqt.executeQuery();

			while (rs.next()) {
				unLieu = new Lieu(rs.getInt("id"), rs.getString("nom"), rs.getString("ville"));
			}
			rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return unLieu;
	}


	/**
	 * Permet l'insertion d'un nouveau lieu
	 * @param obj : hackathon à inserer
	 * @throws DALException
	 */
	public void saveLieu(Lieu obj) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlInsertOne);
			rqt.setString(1, obj.getNom());
			rqt.setString(2, obj.getVille());
			rqt.executeUpdate();
			rqt.close();

			//mise à jour de l'id du hackathon nouvellement créé - le champ name est UNIQUE

			obj.setId(this.getIdByName(obj.getNom(), obj.getVille()));

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
	}


	/**
	 * Permet de mettre à jour uniquement les settings du hackathon dans la base de données
	 * @param obj : hackathon à mettre à jour
	 * @throws DALException
	 */
	public void updateSettings(Lieu obj) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlUpdateOne);
			rqt.setString(1, obj.getNom());
			rqt.setString(2, obj.getVille());
			rqt.setInt(3, obj.getId());
			rqt.executeUpdate();
			rqt.close();

		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}



	public void deleteLieuById(int idlieu) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlDeleteOneById);
			rqt.setInt(1, idlieu);
			rqt.executeUpdate();
			rqt.close();
		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}



	@Override
	public void save(Lieu obj) throws DALException {
		saveLieu(obj);
	}

	@Override
	public void update(Lieu obj) throws DALException {
		updateSettings(obj);
	}


}
