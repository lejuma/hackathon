package fr.siocoliniere.dal.jdbc;


import fr.siocoliniere.bo.Phase;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe DAOHackathon
 * Implémente la classe abstraite DAO
 * BD : Table Phase
 */
public class DAOPhase extends DAO<Phase> {
	/**
	 * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
	 */
	private static DAOPhase instanceDAOPhase;
	private Phase obj;

	/**
	 * Pattern Singleton
	 * @return DAOHackathon
	 */
	public static synchronized DAOPhase getInstance() {
		if (instanceDAOPhase == null) {
			instanceDAOPhase = new DAOPhase();
		}
		return instanceDAOPhase;
	}



	/**
	 * REQUETES
	 */

	private static final String sqlSelectAll = "select * from phase order by id";
	private static final String sqlSelectIdByName = "select id from phase where name = ?";
	private static final String sqlSelectOneById = "select name from phase where id = ?";
	private static final String sqlUpdateOne = "update phase set name = ? where id = ?";
	private static final String sqlInsertOne = "insert into phase(name) values (?)";
	private static final String sqlDeleteOneById = "delete from phase where id = ?";



	/**
	 * Permet de récupérer tous les phases stockées dans la table Phase
	 * @return la liste des phases des hackathons
	 * @throws DALException
	 */
	@Override
	public List<Phase> getAll() throws DALException {

		List<Phase> phaseList = new ArrayList<>();

		try {
			Connection cnx = JdbcTools.getConnection();
			Statement rqt = cnx.createStatement();
			ResultSet rs = rqt.executeQuery(sqlSelectAll);

			while (rs.next()) {
				//instanciation du hackathon
				Phase unePhase = new Phase(rs.getInt("id"), rs.getString("name"));
				phaseList.add(unePhase);
			}
			rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return phaseList;
	}


	/**
	 * Permet de récupérer l'id d'une phase connaissant le nom (contrainte d'unicité sur le nom)
	 * @param nom : nom de la phase recherchée
	 * @return id de la phase
	 * @throws DALException
	 */
	public Integer getIdByName(String nom) throws DALException {

		int idPhase = 1;
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectIdByName);
			rqt.setString(1, nom);
			ResultSet rs = rqt.executeQuery();
			while (rs.next()) {
				//instanciation du hackathon
				idPhase = rs.getInt("id");
			}
			rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return idPhase;
	}


	/**
	 * Permet de récupérer une phase à partir de son id
	 * @param id de la phase
	 * @return la phase dont l'id est transmis en paramètre
	 * @throws DALException
	 */
	@Override
	public Phase getOneById(int id) throws DALException {
		Phase unePhase = null;

		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlSelectOneById);
			rqt.setInt(1, id);
			ResultSet rs = rqt.executeQuery();

			while (rs.next()) {
				unePhase = new Phase(id, rs.getString("name"));
			}
			rqt.close();

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
		return unePhase;
	}


	/**
	 * Permet l'insertion d'une nouvelle phase
	 * @param obj : phase à inserer
	 * @throws DALException
	 */
	public void savePhase(Phase obj) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlInsertOne);
			rqt.setString(1, obj.getName());
			rqt.executeUpdate();
			rqt.close();

			//mise à jour de l'id du hackathon nouvellement créé - le champ name est UNIQUE

			obj.setId(this.getIdByName(obj.getName()));

		} catch (SQLException e) {
			//Exception personnalisée
			throw new DALException(e.getMessage(),e);
		}
	}


	/**
	 * Permet de mettre à jour uniquement une phase dans la base de données
	 * @param obj : phase à mettre à jour
	 * @throws DALException
	 */
	public void updateSettings(Phase obj) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlUpdateOne);
			rqt.setString(1, obj.getName());
			rqt.setInt(2, obj.getId());
			rqt.executeUpdate();
			rqt.close();

		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}



	public void deletePhaseById(int idphase) throws DALException {
		try {
			Connection cnx = JdbcTools.getConnection();
			PreparedStatement rqt = cnx.prepareStatement(sqlDeleteOneById);
			rqt.setInt(1, idphase);
			rqt.executeUpdate();
			rqt.close();
		} catch (SQLException e) {
			throw new DALException(e.getMessage(),e);
		}
	}



	@Override
	public void save(Phase obj) throws DALException {
		savePhase(obj);
	}

	@Override
	public void update(Phase obj) throws DALException {
		updateSettings(obj);
	}
}
