package fr.siocoliniere.view;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bll.LieuController;
import fr.siocoliniere.bll.PhaseController;
import fr.siocoliniere.bll.TypeInscriptionController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Lieu;
import fr.siocoliniere.bo.TypeInscription;
import org.jdatepicker.JDatePicker;

import java.awt.*;
import java.awt.event.*;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;


public class HackathonManagementSettingsForm extends JDialog {

	private Hackathon hackathonManaged;
	private AddLieuForm viewAddLieuForm;
	private AddTypeForm viewAddTypeForm;

	/**
	 * Constructeur
	 * @param owner
	 */
	public HackathonManagementSettingsForm(Window owner) {
		super(owner);

		setModal(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setPreferredSize(new Dimension(550, 500));

		setTitle("Hackat'Orga : Paramètres du Hackathon");

		initComponents();
		initComboLieu();
		initComboTypeInscription();
	}

	/**
	 * Permet d'initialiser la vue pour un hacakthon choisi
	 * @param hackathonManaged
	 */
	public void initHackathonManaged(Hackathon hackathonManaged) {
		this.hackathonManaged = hackathonManaged;

		if (this.hackathonManaged != null) {
			//settings globales : initialisation à partir des données du hackathon
			GregorianCalendar dateBegin = new GregorianCalendar();
			dateBegin.setTime(hackathonManaged.getPeriodBegin());
			GregorianCalendar dateEnd = new GregorianCalendar();
			dateEnd.setTime(hackathonManaged.getPeriodEnd());
			nameTextField.setText(hackathonManaged.getName());
			topicTextField.setText(hackathonManaged.getTopic());
			descriptionTextArea.setText(hackathonManaged.getDescription());
			lieuComboBox.setSelectedIndex(hackathonManaged.getIdLieu()-1);
			periodBeginDatePicker.getModel().setDate(dateBegin.get(Calendar.YEAR), dateBegin.get(Calendar.MONTH), dateBegin.get(Calendar.DAY_OF_MONTH));
			periodEndDatePicker.getModel().setDate(dateEnd.get(Calendar.YEAR), dateEnd.get(Calendar.MONTH), dateEnd.get(Calendar.DAY_OF_MONTH));
			typeComboBox.setSelectedIndex(hackathonManaged.getTypeInscriptionId()-1);
			phaseLabel.setVisible(true);
			phaseTextArea.setText(PhaseController.getInstance().getOneById(hackathonManaged.getIdPhase()).getName());
			phaseTextArea.setVisible(true);
		} else {
			Calendar currentDate = GregorianCalendar.getInstance();
			nameTextField.setText("");
			topicTextField.setText("");
			descriptionTextArea.setText("");
			periodBeginDatePicker.getModel().setDate(currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DAY_OF_MONTH));
			periodEndDatePicker.getModel().setDate(currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DAY_OF_MONTH));
			phaseLabel.setVisible(false);
			phaseTextArea.setVisible(false);
		}
		periodBeginDatePicker.getModel().setSelected(true);
		periodEndDatePicker.getModel().setSelected(true);
	}


	private void initComboLieu() {
		//!!! Info technique : un objet de type ComboBox fonctionne avec un objet de type DefaultComboBoxModel qui va contenir les données

		List<Lieu> lesLieux = HackathonController.getInstance().getAllLieu();

		// création d'un model à partir de la liste d'objets Lieu
		DefaultComboBoxModel<Lieu> modelCombo = new DefaultComboBoxModel<>();

		for (Lieu unLieu : lesLieux) {
			modelCombo.addElement(unLieu);
		}
		//rattachement du DefaultComboBoxModel au composant graphique ComboBox
		lieuComboBox.setModel(modelCombo);
	}




	 private void initComboTypeInscription() {
		 //!!! Info technique : un objet de type ComboBox fonctionne avec un objet de type DefaultComboBoxModel qui va contenir les données

		 List<TypeInscription> types = HackathonController.getInstance().getAllTypeInscription();

		 // création d'un model à partir de la liste d'objets TypeInscription
		 DefaultComboBoxModel<TypeInscription> modelCombo = new DefaultComboBoxModel<>();

		 for (TypeInscription type : types) {
			 modelCombo.addElement(type);
		 }
		 //rattachement du DefaultComboBoxModel au composant graphique ComboBox
		 typeComboBox.setModel(modelCombo);
	 }


	/**
	 * Permet de sauvegarder les settings du hackathon
	 * Appelée : lors du clic sur le bouton 'Save'
	 */
	private void saveButtonActionPerformed(ActionEvent e) {
		Calendar avant = (GregorianCalendar) periodBeginDatePicker.getModel().getValue();
		Calendar apres = (GregorianCalendar) periodEndDatePicker.getModel().getValue();
        if (!(nameTextField.getText().isBlank()) && !(topicTextField.getText().isBlank()) && !(descriptionTextArea.getText().isBlank()) && avant.before(apres)) {
			Date dateBegin = new Date(((GregorianCalendar) periodBeginDatePicker.getModel().getValue()).getTimeInMillis());
			Date dateEnd = new Date(((GregorianCalendar) periodEndDatePicker.getModel().getValue()).getTimeInMillis());
			Lieu unLieu = (Lieu) lieuComboBox.getSelectedItem();
			TypeInscription unType = (TypeInscription) typeComboBox.getSelectedItem();

			int idPhase = 1;
			if (phaseTextArea.getText() != null) {
				idPhase = PhaseController.getInstance().getIdByName(phaseTextArea.getText());
			}
			assert unLieu != null;
			assert unType != null;
			HackathonController.getInstance().saveHackathonStandalone(hackathonManaged, nameTextField.getText(), topicTextField.getText(), descriptionTextArea.getText(), dateBegin, dateEnd, HackathonController.getInstance().getIdLieuByNom(unLieu.getNom(), unLieu.getVille()), HackathonController.getInstance().getIdTypeInscriptionByName(unType.getName()), idPhase);
            this.dispose();
        } else if (apres.before(avant) || avant.equals(apres)) {
			errorTextArea.setText("La date de fin du Hackathon doit être postérieure \nà la date de début du Hackathon");
			errorTextArea.setVisible(true);
		} else {
			errorTextArea.setText("Certains champs obligatoires ont été laissés vides");
            errorTextArea.setVisible(true);
        }
	}

    private void thisWindowGainedFocus(WindowEvent e) {
		errorTextArea.setVisible(false);
		HackathonController.getInstance().update();
		LieuController.getInstance().update();
		TypeInscriptionController.getInstance().update();
		initComboLieu();
		initComboTypeInscription();
    }

    /**
     * Appelée : lors du clic sur le bouton Cancel
     */
    private void cancelButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }

	private void addLieuButtonActionPerformed(ActionEvent e) {
		this.viewAddLieuForm = new AddLieuForm(this);
		this.viewAddLieuForm.setVisible(true);
	}

	private void addTypeButtonActionPerformed(ActionEvent e) {
		this.viewAddTypeForm = new AddTypeForm(this);
		this.viewAddTypeForm.setVisible(true);
	}


	/**
	 * JFormDesigner - DO NOT MODIFY
	 */
	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		nameLabel = new JLabel();
		nameTextField = new JTextField();
		topicLabel = new JLabel();
		topicTextField = new JTextField();
		descriptionLabel = new JLabel();
		descriptionScrollPane = new JScrollPane();
		descriptionTextArea = new JTextArea();
		townLabel = new JLabel();
		lieuComboBox = new JComboBox();
		addLieuButton = new JButton();
		periodBeginLabel = new JLabel();
		periodBeginDatePicker = new JDatePicker();
		periodEndLabel = new JLabel();
		periodEndDatePicker = new JDatePicker();
		typeLabel = new JLabel();
		typeComboBox = new JComboBox();
		addTypeButton = new JButton();
		phaseLabel = new JLabel();
		phaseTextArea = new JTextArea();
		buttonBar = new JPanel();
		errorTextArea = new JTextArea();
		saveButton = new JButton();
		cancelButton = new JButton();

		//======== this ========
		addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				thisWindowGainedFocus(e);
			}
		});
		var contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout(0, 10));

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setLayout(new BorderLayout(0, 10));

			//======== contentPanel ========
			{
				contentPanel.setLayout(new GridBagLayout());
				((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {0, 0, 0, 39, 0, 0};
				((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 31, 24, 102, 19, 0, 39, 35, 35, 0, 0};
				((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 0.0, 0.0, 1.0E-4};
				((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

				//---- nameLabel ----
				nameLabel.setText("Nom *");
				contentPanel.add(nameLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.EAST, GridBagConstraints.NONE,
					new Insets(0, 10, 10, 5), 0, 0));
				contentPanel.add(nameTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 10, 5), 0, 0));

				//---- topicLabel ----
				topicLabel.setText("Sujet *");
				contentPanel.add(topicLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.EAST, GridBagConstraints.NONE,
					new Insets(0, 10, 10, 5), 0, 0));
				contentPanel.add(topicTextField, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 10, 5), 0, 0));

				//---- descriptionLabel ----
				descriptionLabel.setText("Description *");
				contentPanel.add(descriptionLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
					GridBagConstraints.EAST, GridBagConstraints.NONE,
					new Insets(0, 10, 10, 5), 0, 0));

				//======== descriptionScrollPane ========
				{
					descriptionScrollPane.setViewportView(descriptionTextArea);
				}
				contentPanel.add(descriptionScrollPane, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 20, 5), 0, 0));

				//---- townLabel ----
				townLabel.setText("Lieu *");
				contentPanel.add(townLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
					GridBagConstraints.EAST, GridBagConstraints.NONE,
					new Insets(0, 10, 10, 5), 0, 0));
				contentPanel.add(lieuComboBox, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 10, 5), 0, 0));

				//---- addLieuButton ----
				addLieuButton.setText("+");
				addLieuButton.addActionListener(e -> addLieuButtonActionPerformed(e));
				contentPanel.add(addLieuButton, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
					new Insets(0, 0, 10, 5), 0, 0));

				//---- periodBeginLabel ----
				periodBeginLabel.setText("De *");
				contentPanel.add(periodBeginLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
					GridBagConstraints.EAST, GridBagConstraints.NONE,
					new Insets(0, 10, 10, 5), 0, 0));
				contentPanel.add(periodBeginDatePicker, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 10, 5), 0, 0));

				//---- periodEndLabel ----
				periodEndLabel.setText("\u00c0 *");
				contentPanel.add(periodEndLabel, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
					GridBagConstraints.EAST, GridBagConstraints.NONE,
					new Insets(0, 0, 10, 5), 0, 0));
				contentPanel.add(periodEndDatePicker, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 10, 5), 0, 0));

				//---- typeLabel ----
				typeLabel.setText("Type d'inscription *");
				contentPanel.add(typeLabel, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
					GridBagConstraints.EAST, GridBagConstraints.NONE,
					new Insets(0, 0, 10, 5), 0, 0));
				contentPanel.add(typeComboBox, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 10, 5), 0, 0));

				//---- addTypeButton ----
				addTypeButton.setText("+");
				addTypeButton.addActionListener(e -> addTypeButtonActionPerformed(e));
				contentPanel.add(addTypeButton, new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
					new Insets(0, 0, 10, 5), 0, 0));

				//---- phaseLabel ----
				phaseLabel.setText("Phase actuelle");
				contentPanel.add(phaseLabel, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
					GridBagConstraints.EAST, GridBagConstraints.NONE,
					new Insets(0, 0, 10, 5), 0, 0));

				//---- phaseTextArea ----
				phaseTextArea.setEditable(false);
				contentPanel.add(phaseTextArea, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 10, 5), 0, 0));
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			//======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {68, 456, 54};
				((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0};

				//---- errorTextArea ----
				errorTextArea.setEditable(false);
				errorTextArea.setForeground(new Color(0xcc0000));
				errorTextArea.setText("Erreur");
				errorTextArea.setRows(2);
				errorTextArea.setFont(new Font("Noto Sans", Font.BOLD, 12));
				buttonBar.add(errorTextArea, new GridBagConstraints(1, 0, 1, 2, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- saveButton ----
				saveButton.setText("Sauvegarder");
				saveButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				saveButton.addActionListener(e -> saveButtonActionPerformed(e));
				buttonBar.add(saveButton, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- cancelButton ----
				cancelButton.setText("Annuler");
				cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
				buttonBar.add(cancelButton, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 0), 0, 0));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}


	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JLabel nameLabel;
	private JTextField nameTextField;
	private JLabel topicLabel;
	private JTextField topicTextField;
	private JLabel descriptionLabel;
	private JScrollPane descriptionScrollPane;
	private JTextArea descriptionTextArea;
	private JLabel townLabel;
	private JComboBox lieuComboBox;
	private JButton addLieuButton;
	private JLabel periodBeginLabel;
	private JDatePicker periodBeginDatePicker;
	private JLabel periodEndLabel;
	private JDatePicker periodEndDatePicker;
	private JLabel typeLabel;
	private JComboBox typeComboBox;
	private JButton addTypeButton;
	private JLabel phaseLabel;
	private JTextArea phaseTextArea;
	private JPanel buttonBar;
	private JTextArea errorTextArea;
	private JButton saveButton;
	private JButton cancelButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
