package fr.siocoliniere.view;
import fr.siocoliniere.bo.Hackathon;

import javax.swing.event.*;

import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.view.HackathonManagementJuryForm;
import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Hackathon;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;


public class HackathonManagementForm extends JDialog {

	private Hackathon hackathonManaged;

    // vues liées
    private HackathonManagementSettingsForm viewHackathonManageSettings;
    private HackathonManagementJuryForm viewHackathonManageJury;
    private HackathonManagementAnimateurForm viewHackathonManageAnim;
    private HackathonManagementExpertForm viewHackathonManageExpert;

	public HackathonManagementForm() {

		this.hackathonManaged = null;

        setPreferredSize(new Dimension(350, 350));
        setLocation(500, 500);
        setModal(true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        pack();
        initComponents();

        // instanciation des vues liées
        viewHackathonManageSettings = new HackathonManagementSettingsForm(this);
        viewHackathonManageJury = new HackathonManagementJuryForm(this);
        viewHackathonManageAnim = new HackathonManagementAnimateurForm(this);
        viewHackathonManageExpert = new HackathonManagementExpertForm(this);

        //masquage des vues liées
        viewHackathonManageSettings.setVisible(false);
        viewHackathonManageJury.setVisible(false);
        viewHackathonManageAnim.setVisible(false);
        viewHackathonManageExpert.setVisible(false);
    }

	/**
	 * Permet de valoriser le hackathon à configurer
	 * @param hackathon le hackathon valorisé
	 */
	public void setHackathon(Hackathon hackathon) {
		this.hackathonManaged = hackathon;
		if (this.hackathonManaged != null) {
			setTitle("Hackat'Orga : " + this.hackathonManaged.getName());
		} else {
			setTitle("Hackat'Orga : New hackathon");
		}
	}


	 // METHODES RATTACHEES A LA GESTION AUX EVENEMENTS
	/**
	 * Permet de quitter l'application
	 * Appelée : lors du clic sur le bouton Close
	 */
	private void closeButtonActionPerformed(ActionEvent e) {
		this.setVisible(false);
	}

	/**
	 * Permet de paramétrer les settings du Hackathon
	 * Appelée : lors du clic sur le bouton Settings
	 */
	private void settingsButtonActionPerformed(ActionEvent e) {
		//affichage de la vue pour le hackathon concerné
		viewHackathonManageSettings.initHackathonManaged(hackathonManaged);
		viewHackathonManageSettings.setVisible(true);
	}

	private void juryButtonActionPerformed(ActionEvent e) {
		//affichage de la vue pour le hackathon concerné
		viewHackathonManageJury.initHackathonManaged(hackathonManaged);
		viewHackathonManageJury.setVisible(true);
	}

	private void animButtonActionPerformed(ActionEvent e) {
		//affichage de la vue pour le hackathon concerné
		viewHackathonManageAnim.initHackathonManaged(hackathonManaged);
		viewHackathonManageAnim.setVisible(true);
	}

    private void expertButtonActionPerformed(ActionEvent e) {
        //affichage de la vue pour le hackathon concerné
        viewHackathonManageExpert.initHackathonManaged(hackathonManaged);
        viewHackathonManageExpert.setVisible(true);
    }

    /**
     * JFormDesigner - DO NOT MODIFY
     */

    private void delete(ActionEvent e) {
        ConfirmForm confirmView = new ConfirmForm(this,"Confirmation", "Are you sure to delete this Hackathon");
        confirmView.pack();
        confirmView.setVisible(true);
        if (confirmView.isAnswer()) {
            HackathonController.getInstance().deleteHackathon(hackathonManaged);
            this.setVisible(false);
        }
    }


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		settingsButton = new JButton();
		juryButton = new JButton();
		animButton = new JButton();
		expertButton = new JButton();
		buttonBar = new JPanel();
		label1 = new JLabel();
		deleteButton = new JButton();
		closeButton = new JButton();

		//======== this ========
		var contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setLayout(new BorderLayout());

			//======== contentPanel ========
			{
				contentPanel.setLayout(new GridBagLayout());
				((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
				((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {8, 37, 0, 0, 0, 0, 0, 29, 0};
				((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0, 1.0E-4};
				((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};

				//---- settingsButton ----
				settingsButton.setText("Informations du hackathon");
				settingsButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				settingsButton.addActionListener(e -> settingsButtonActionPerformed(e));
				contentPanel.add(settingsButton, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 4, 4), 0, 0));

				//---- juryButton ----
				juryButton.setText("Les jur\u00e9s");
				juryButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				juryButton.addActionListener(e -> juryButtonActionPerformed(e));
				contentPanel.add(juryButton, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 4, 4), 0, 0));

				//---- animButton ----
				animButton.setText("Les animateurs");
				animButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				animButton.addActionListener(e -> animButtonActionPerformed(e));
				contentPanel.add(animButton, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 4, 4), 0, 0));

				//---- expertButton ----
				expertButton.setText("Les experts");
				expertButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				expertButton.addActionListener(e -> expertButtonActionPerformed(e));
				contentPanel.add(expertButton, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 4, 4), 0, 0));
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			//======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 80};
				((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 0.0, 0.0};

				//---- label1 ----
				label1.setIcon(new ImageIcon(getClass().getResource("/r2d2.png")));
				buttonBar.add(label1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- deleteButton ----
				deleteButton.setText("Supprimer");
				deleteButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				deleteButton.addActionListener(e -> delete(e));
				buttonBar.add(deleteButton, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- closeButton ----
				closeButton.setText("Fermer");
				closeButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				closeButton.addActionListener(e -> closeButtonActionPerformed(e));
				buttonBar.add(closeButton, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JButton settingsButton;
	private JButton juryButton;
	private JButton animButton;
	private JButton expertButton;
	private JPanel buttonBar;
	private JLabel label1;
	private JButton deleteButton;
	private JButton closeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
