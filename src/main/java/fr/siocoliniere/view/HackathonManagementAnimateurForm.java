/*
 * Created by JFormDesigner on Fri Sep 02 14:31:59 CEST 2022
 */

package fr.siocoliniere.view;

import java.beans.*;
import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bll.MemberController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.*;

import java.util.List;

/**
 * @author unknown
 */
public class HackathonManagementAnimateurForm extends JDialog {
    private HackathonManagementCreateNewAnimateurForm  viewHackathonManagementCreateNewAnimateur;
    private Hackathon hackathonManaged;

    private void initComboBoxMembers() {
        //!!! Info technique : un objet de type ComboBox fonctionne avec un objet de type DefaultComboBoxModel qui va contenir les données

        // récupération des membres
        List<Member> members = MemberController.getInstance().getNonRoleMember(3);

        // création d'un model à partir de la liste d'objets hackathons
        if (members != null) {
            DefaultComboBoxModel<Member> modelCombo = new DefaultComboBoxModel<>();
            for (Member member : members) {
                modelCombo.addElement(member);
            }
            //rattachement du DefaultComboBoxModel au composant graphique ComboBox
            search_new_animator_combobox.setModel(modelCombo);

        } else {
            //gestion d'une erreur de récupération des données
            this.addExistingMemberButton.setEnabled(false);
        }
    }

    public void initHackathonManaged(Hackathon hackathonManaged) {
        this.hackathonManaged = hackathonManaged;

		if (this.hackathonManaged != null) {
			// initialisation de la JList des animateurs existants
			initAnimList(hackathonManaged.getAnimMembers());
			initPotentialAnimList(HackathonController.getInstance().getPotentialAnim(hackathonManaged.getId()));
		}
		else{
			initAnimList(new ArrayList<>());
			initPotentialAnimList(HackathonController.getInstance().getPotentialAnim(0));
		}
	}

	public HackathonManagementAnimateurForm(Window owner) {
		super(owner);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setPreferredSize(new Dimension(750, 500));

		setTitle("Hackat'Orga : Animator ");

        initComponents();
        initComboBoxMembers();
        // instanciation des vues liées
        viewHackathonManagementCreateNewAnimateur = new HackathonManagementCreateNewAnimateurForm(this);

        //masquage des vues liées
        viewHackathonManagementCreateNewAnimateur.setVisible(false);

    }
    /**
     * Permet de valoriser la Jlist des membres actuels du hackathon
     * @param animators liste des membres animateur
     */
    private void initAnimList(java.util.List<Member> animators) {
        DefaultListModel<Member> modelAnimList = new DefaultListModel<>();
        for (Member member : animators) {
            modelAnimList.addElement(member);
        }
        //rattachement du DefaultListModel au composant graphique JList
        this.animList.setModel(modelAnimList);
    }

	private void initPotentialAnimList(List<Member> memberList) {
		DefaultListModel<Member> modelPotentialAnimList = new DefaultListModel<>();
		for (Member member : memberList) {
			modelPotentialAnimList.addElement(member);
		}
		//rattachement du DefaultListModel au composant graphique JList
		this.potentialAnimList.setModel(modelPotentialAnimList);
	}

    private void removeButtonActionPerformed(ActionEvent e) {
        //récupération de l'animateur sélectionné
        Member animSelected = (Member) animList.getSelectedValue();

        //suppression du membre sélectionné dans le model rattaché à la Jlist gérant les animateurs du hackathon
        DefaultListModel<Member> modelAnimList = (DefaultListModel<Member>) animList.getModel();
        modelAnimList.removeElement(animSelected);

        //ajout du membre sélectionné au model rattaché à la JList gérant les membres animateurs potentiels
        DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) potentialAnimList.getModel();
        modelMemberList.addElement(animSelected);
    }
    private void addButtonActionPerformed(ActionEvent e) {

        //récupération de l'animateur sélectionné
        Member animSelected = (Member) potentialAnimList.getSelectedValue();

        //suppression du membre sélectionné dans le model rattaché à la Jlist gérant les animateurs potentiels du hackathon
        DefaultListModel<Member> modelAnimList = (DefaultListModel<Member>) potentialAnimList.getModel();
        modelAnimList.removeElement(animSelected);

        //ajout du membre sélectionné au model rattaché à la JList gérant les animateurs
        DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) animList.getModel();
        modelMemberList.addElement(animSelected);
    }

    private void addNewButtonActionPerformed(ActionEvent e) {
        // Construction de la liste de animateur à sauvegarder
        List<Member> animators = new ArrayList<>();
        for (int i =0; i< animList.getModel().getSize(); i++){
            Member anim = (Member) animList.getModel().getElementAt(i);
            animators.add(anim);
        }

        //affichage de la vue pour le hackathon concerné
        viewHackathonManagementCreateNewAnimateur.initHackathonManaged(hackathonManaged);
        viewHackathonManagementCreateNewAnimateur.setVisible(true);
    }

    private void addAnimButtonActionPerformed(ActionEvent e) {
        //récupération du hackathon sélectionné
        Member memberManaged = (Member) search_new_animator_combobox.getSelectedItem();

        //suppression du membre sélectionné dans le model rattaché à la Jlist gérant les animateurs potentiels du hackathon
        search_new_animator_combobox.removeItem(memberManaged);

        if (memberManaged != null) {
            //ajout du membre sélectionné au model rattaché à la JList gérant les animateurs
            DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) animList.getModel();
            modelMemberList.addElement(memberManaged);
        }
    }

    private void SaveButtonActionPerformed(ActionEvent e) {
        // Construction de la liste de animateur à sauvegarder
        List<Member> animators = new ArrayList<>();
        for (int i =0; i< animList.getModel().getSize(); i++){
            Member anim = (Member) animList.getModel().getElementAt(i);
            animators.add(anim);
        }

        //persistence
        HackathonController.getInstance().saveParticipationAnimByIdHackathon(hackathonManaged,animators);

        hackathonManaged.setAnimMembers(animators);

        this.dispose();

    }
	private void cancelButtonActionPerformed(ActionEvent e) {
		this.dispose();
	}

    private void thisWindowGainedFocus(WindowEvent e) {
        HackathonController.getInstance().update();
        // initHackathonManaged(hackathonManaged);

        //ajout du membre tout juste créé au model rattaché à la JList gérant les animateurs
        DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) animList.getModel();
        modelMemberList.addElement(hackathonManaged.getTmpMember());
        hackathonManaged.setTmpMember(null);
    }
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		existingLabel = new JLabel();
		potentialLabel = new JLabel();
		animMemberScrollPane = new JScrollPane();
		animList = new JList();
		ARButtonPanel = new JPanel();
		addButton = new JButton();
		removeButton = new JButton();
		memberScrollPane = new JScrollPane();
		potentialAnimList = new JList();
		buttonBar = new JPanel();
		label1 = new JLabel();
		search_new_animator_combobox = new JComboBox();
		addExistingMemberButton = new JButton();
		button2 = new JButton();
		SaveButton = new JButton();
		cancelButton = new JButton();

		//======== this ========
		setMinimumSize(null);
		addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				thisWindowGainedFocus(e);
			}
		});
		var contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setLayout(new BorderLayout());

			//======== contentPanel ========
			{
				contentPanel.setLayout(new GridBagLayout());
				((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {35, 112, 0, 119, 0, 0};
				((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 239, 0};
				((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};
				((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

				//---- existingLabel ----
				existingLabel.setText("Animateurs s\u00e9lectionn\u00e9s");
				contentPanel.add(existingLabel, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- potentialLabel ----
				potentialLabel.setText("Animateurs potentiels");
				contentPanel.add(potentialLabel, new GridBagConstraints(3, 0, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 0), 0, 0));

				//======== animMemberScrollPane ========
				{
					animMemberScrollPane.setViewportView(animList);
				}
				contentPanel.add(animMemberScrollPane, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(5, 5, 0, 10), 0, 0));

				//======== ARButtonPanel ========
				{
					ARButtonPanel.setLayout(new GridBagLayout());
					((GridBagLayout)ARButtonPanel.getLayout()).columnWidths = new int[] {0, 0};
					((GridBagLayout)ARButtonPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 42, 0};
					((GridBagLayout)ARButtonPanel.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
					((GridBagLayout)ARButtonPanel.getLayout()).rowWeights = new double[] {1.0, 0.0, 0.0, 1.0, 1.0, 1.0E-4};

					//---- addButton ----
					addButton.setIcon(new ImageIcon(getClass().getResource("/arrowG.png")));
					addButton.setText("Ajouter");
					addButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					addButton.addActionListener(e -> addButtonActionPerformed(e));
					ARButtonPanel.add(addButton, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
						GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
						new Insets(0, 0, 5, 0), 0, 0));

					//---- removeButton ----
					removeButton.setIcon(new ImageIcon(getClass().getResource("/arrowD.png")));
					removeButton.setText("Supprimer");
					removeButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					removeButton.addActionListener(e -> removeButtonActionPerformed(e));
					ARButtonPanel.add(removeButton, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
						GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
						new Insets(0, 0, 5, 0), 0, 0));
				}
				contentPanel.add(ARButtonPanel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//======== memberScrollPane ========
				{
					memberScrollPane.setViewportView(potentialAnimList);
				}
				contentPanel.add(memberScrollPane, new GridBagConstraints(3, 1, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			//======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {146, 58, 58, 66, 55};

				//---- label1 ----
				label1.setText("Ajouter un membre en tant qu'animateur :");
				buttonBar.add(label1, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- search_new_animator_combobox ----
				search_new_animator_combobox.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				buttonBar.add(search_new_animator_combobox, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- addExistingMemberButton ----
				addExistingMemberButton.setText("Ajouter");
				addExistingMemberButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				addExistingMemberButton.addActionListener(e -> addAnimButtonActionPerformed(e));
				buttonBar.add(addExistingMemberButton, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- button2 ----
				button2.setText("Cr\u00e9er un nouvel animateur");
				button2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				button2.addActionListener(e -> addNewButtonActionPerformed(e));
				buttonBar.add(button2, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- SaveButton ----
				SaveButton.setText("Sauvegarder");
				SaveButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				SaveButton.addActionListener(e -> SaveButtonActionPerformed(e));
				buttonBar.add(SaveButton, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- cancelButton ----
				cancelButton.setText("Annuler");
				cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
				buttonBar.add(cancelButton, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		pack();
		setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JLabel existingLabel;
	private JLabel potentialLabel;
	private JScrollPane animMemberScrollPane;
	private JList animList;
	private JPanel ARButtonPanel;
	private JButton addButton;
	private JButton removeButton;
	private JScrollPane memberScrollPane;
	private JList potentialAnimList;
	private JPanel buttonBar;
	private JLabel label1;
	private JComboBox search_new_animator_combobox;
	private JButton addExistingMemberButton;
	private JButton button2;
	private JButton SaveButton;
	private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
