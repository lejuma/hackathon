/*
 * Created by JFormDesigner on Tue Nov 08 14:46:47 CET 2022
 */

package fr.siocoliniere.view;

import fr.siocoliniere.bll.LieuController;
import fr.siocoliniere.bo.Lieu;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * @author unknown
 */
public class AddLieuForm extends JDialog {
	private Lieu lieuManaged;

	public AddLieuForm(Window owner) {
		super(owner);
		setPreferredSize(new Dimension(350, 110));
		initComponents();
	}

	/**
	 * Permet d'initialiser la vue pour un lieu choisi
	 * @param lieuManaged
	 */
	public void initLieuManaged(Lieu lieuManaged) {
		this.lieuManaged = lieuManaged;
	}

	private void thisWindowOpened(WindowEvent e) {
		this.locationTextField.setText("");
		this.townTextField.setText("");
	}

	private void cancelButtonActionPerformed(ActionEvent e) {
		this.dispose();
	}

	private void addButtonActionPerformed(ActionEvent e) {
		if (!(locationTextField.getText().isBlank()) && !(townTextField.getText().isBlank())) {
			LieuController.getInstance().saveLieuStandalone(lieuManaged, locationTextField.getText(), townTextField.getText());
			this.dispose();
		}
	}



	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
		locationLabel = new JLabel();
		locationTextField = new JTextField();
		townLabel = new JLabel();
		townTextField = new JTextField();
		addButton = new JButton();
		cancelButton = new JButton();

		//======== this ========
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				thisWindowOpened(e);
			}
		});
		var contentPane = getContentPane();
		contentPane.setLayout(new GridBagLayout());
		((GridBagLayout)contentPane.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
		((GridBagLayout)contentPane.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
		((GridBagLayout)contentPane.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0, 0.0, 1.0E-4};
		((GridBagLayout)contentPane.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

		//---- locationLabel ----
		locationLabel.setText("Lieu*");
		contentPane.add(locationLabel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
			GridBagConstraints.EAST, GridBagConstraints.NONE,
			new Insets(0, 0, 5, 5), 0, 0));
		contentPane.add(locationTextField, new GridBagConstraints(2, 1, 2, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 5, 5), 0, 0));

		//---- townLabel ----
		townLabel.setText("Ville*");
		contentPane.add(townLabel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
			GridBagConstraints.EAST, GridBagConstraints.NONE,
			new Insets(0, 0, 5, 5), 0, 0));
		contentPane.add(townTextField, new GridBagConstraints(2, 2, 2, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 5, 5), 0, 0));

		//---- addButton ----
		addButton.setText("Ajouter");
		addButton.addActionListener(e -> addButtonActionPerformed(e));
		contentPane.add(addButton, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 5, 5), 0, 0));

		//---- cancelButton ----
		cancelButton.setText("Annuler");
		cancelButton.setPreferredSize(null);
		cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
		contentPane.add(cancelButton, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 5, 5), 0, 0));
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
	private JLabel locationLabel;
	private JTextField locationTextField;
	private JLabel townLabel;
	private JTextField townTextField;
	private JButton addButton;
	private JButton cancelButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
