/*
 * Created by JFormDesigner on Tue Sep 20 14:58:19 CEST 2022
 */

package fr.siocoliniere.view;

import fr.siocoliniere.bll.MemberController;
import fr.siocoliniere.bo.Member;

import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.border.*;


public class MemberManagementForm extends JDialog {
    private Member memberManaged;

    public MemberManagementForm() {
        this.memberManaged = null;
		setPreferredSize(new Dimension(700, 400));
        setLocation(500, 500);
        setModal(true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        pack();

        setTitle("Hackat'Orga : Member Data");

        initComponents();
    }

    /**
     * Permet d'initialiser la vue pour un membre choisi
     * @param memberManaged
     */
    public void initMemberManaged(Member memberManaged) {
        this.memberManaged = memberManaged;

        if (this.memberManaged != null) {
            setTitle("Hackat'Orga : " + this.memberManaged.getFirstname() + " " + this.memberManaged.getLastname());
            firstnameTextField.setText(memberManaged.getFirstname());
            lastnameTextField.setText(memberManaged.getLastname());
        } else {
            setTitle("Hackat'Orga : Nouveau membre");
            firstnameTextField.setText("");
            lastnameTextField.setText("");
        }
    }

    private void SaveButtonActionPerformed(ActionEvent e) {
		if (!(firstnameTextField.getText().isBlank()) && !(lastnameTextField.getText().isBlank()) && !(mailTextField.getText().isBlank())) {
			MemberController.getInstance().saveMemberStandalone(memberManaged, firstnameTextField.getText(), lastnameTextField.getText(), mailTextField.getText());
			this.dispose();
		}
    }

    private void closeButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		label1 = new JLabel();
		firstnameLabel = new JLabel();
		firstnameTextField = new JTextField();
		lastnameLabel = new JLabel();
		lastnameTextField = new JTextField();
		mailLabel = new JLabel();
		mailTextField = new JTextField();
		buttonBar = new JPanel();
		okButton = new JButton();
		cancelButton = new JButton();

		//======== this ========
		setPreferredSize(null);
		setMinimumSize(new Dimension(450, 25));
		var contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setLayout(new BorderLayout());

			//======== contentPanel ========
			{
				contentPanel.setLayout(new GridBagLayout());
				((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0};

				//---- label1 ----
				label1.setText("Entrez le pr\u00e9nom, le nom et le mail du nouveau membre");
				contentPanel.add(label1, new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.NONE,
					new Insets(0, 0, 5, 0), 0, 0));

				//---- firstnameLabel ----
				firstnameLabel.setText("Pr\u00e9nom*");
				contentPanel.add(firstnameLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(0, 0, 5, 5), 0, 0));
				contentPanel.add(firstnameTextField, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 0), 0, 0));

				//---- lastnameLabel ----
				lastnameLabel.setText("Nom*");
				contentPanel.add(lastnameLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(0, 0, 5, 5), 0, 0));
				contentPanel.add(lastnameTextField, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 0), 0, 0));

				//---- mailLabel ----
				mailLabel.setText("E-mail*");
				contentPanel.add(mailLabel, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(0, 0, 5, 5), 0, 0));
				contentPanel.add(mailTextField, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 0), 0, 0));
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			//======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
				((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

				//---- okButton ----
				okButton.setText("Sauvegarder");
				okButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				okButton.addActionListener(e -> SaveButtonActionPerformed(e));
				buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- cancelButton ----
				cancelButton.setText("Annuler");
				cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				cancelButton.addActionListener(e -> closeButtonActionPerformed(e));
				buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		setSize(400, 300);
		setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JLabel label1;
	private JLabel firstnameLabel;
	private JTextField firstnameTextField;
	private JLabel lastnameLabel;
	private JTextField lastnameTextField;
	private JLabel mailLabel;
	private JTextField mailTextField;
	private JPanel buttonBar;
	private JButton okButton;
	private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
