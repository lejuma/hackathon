/*
 * Created by JFormDesigner on Tue Sep 06 11:05:03 CEST 2022
 */

package fr.siocoliniere.view;

import java.awt.event.*;
import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bll.MemberController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * @author unknown
 */
public class HackathonManagementCreateNewExpertForm extends JDialog {
    Hackathon hackathonManaged;
    public HackathonManagementCreateNewExpertForm(Window owner) {
        super(owner);
		setPreferredSize(new Dimension(700, 400));
        initComponents();
    }

    /**
     * Permet d'initialiser la vue pour un hacakthon choisi
     * @param hackathonManaged
     */
    public void initHackathonManaged(Hackathon hackathonManaged) {
        this.hackathonManaged = hackathonManaged;
    }

    private void thisWindowGainedFocus(WindowEvent e) {
        firstnameTextField.setText("");
        lastnameTextField.setText("");
        mailTextField.setText("");
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		pleaseLabel = new JLabel();
		assignLabel = new JLabel();
		firstnameLabel = new JLabel();
		firstnameTextField = new JTextField();
		lastnameLabel = new JLabel();
		lastnameTextField = new JTextField();
		mailLabel = new JLabel();
		mailTextField = new JTextField();
		saveCancelButtonPanel = new JPanel();
		saveButton = new JButton();
		cancelButton = new JButton();

		//======== this ========
		setPreferredSize(null);
		setMinimumSize(new Dimension(450, 25));
		addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				thisWindowGainedFocus(e);
			}
		});
		var contentPane = getContentPane();
		contentPane.setLayout(new MigLayout(
			"fillx,hidemode 3,align center top",
			// columns
			"[fill]" +
			"[fill]",
			// rows
			"[grow]" +
			"[grow]" +
			"[grow]" +
			"[42,grow]" +
			"[grow]" +
			"[grow]" +
			"[17]" +
			"[96,grow]" +
			"[]"));

		//---- pleaseLabel ----
		pleaseLabel.setText("Ins\u00e9rez le pr\u00e9nom et le nom du nouveau membre.");
		contentPane.add(pleaseLabel, "cell 0 1 2 1,alignx center,growx 0");

		//---- assignLabel ----
		assignLabel.setText("Il sera un expert de cet Hackathon.");
		contentPane.add(assignLabel, "cell 0 2 2 1,alignx center,growx 0");

		//---- firstnameLabel ----
		firstnameLabel.setText("Pr\u00e9nom*");
		contentPane.add(firstnameLabel, "cell 0 4,aligny center,growy 0");

		//---- firstnameTextField ----
		firstnameTextField.setMaximumSize(new Dimension(500, 500));
		contentPane.add(firstnameTextField, "cell 1 4,aligny center,growy 0");

		//---- lastnameLabel ----
		lastnameLabel.setText("Nom*");
		contentPane.add(lastnameLabel, "cell 0 5,aligny center,growy 0");
		contentPane.add(lastnameTextField, "cell 1 5,aligny center,growy 0");

		//---- mailLabel ----
		mailLabel.setText("Mail*");
		contentPane.add(mailLabel, "cell 0 6,aligny center,growy 0");
		contentPane.add(mailTextField, "cell 1 6,aligny center,growy 0");

		//======== saveCancelButtonPanel ========
		{
			saveCancelButtonPanel.setLayout(new MigLayout(
				"hidemode 3",
				// columns
				"[376,fill]" +
				"[376,fill]" +
				"[376,fill]",
				// rows
				"[]"));

			//---- saveButton ----
			saveButton.setText("Sauvegarder");
			saveButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			saveButton.addActionListener(e -> SaveButtonActionPerformed(e));
			saveCancelButtonPanel.add(saveButton, "cell 1 0");

			//---- cancelButton ----
			cancelButton.setText("Annuler");
			cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
			saveCancelButtonPanel.add(cancelButton, "cell 2 0");
		}
		contentPane.add(saveCancelButtonPanel, "cell 0 7 2 1");
		setSize(400, 335);
		setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    private void SaveButtonActionPerformed(ActionEvent e) {
        HackathonController.getInstance().saveMember(firstnameTextField.getText(), lastnameTextField.getText(), mailTextField.getText());

        // obtention des id du hackathon et du nouveau membre
        int idmem = HackathonController.getInstance().getOneMemberIDByName(firstnameTextField.getText(), lastnameTextField.getText(), mailTextField.getText());
        int idhack = hackathonManaged.getId();

		if (!(firstnameTextField.getText().isBlank()) && !(lastnameTextField.getText().isBlank()) && !(mailTextField.getText().isBlank())) {
			Member newMember = new Member (idmem, firstnameTextField.getText(), lastnameTextField.getText(), mailTextField.getText());

			// permet de passer newMember à la fenêtre HackathonManagementExpertForm
			hackathonManaged.setTmpMember(newMember);

			// Enregistrement en base de donnée et dans la liste du programme
			// HackathonController.getInstance().saveParticipationExpert(idhack, idmem);
			// hackathonManaged.addExpertMember(newMember);

			this.dispose();
		}
    }

    /**
     * Appelée : lors du clic sur le bouton Cancel
     */
    private void cancelButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JLabel pleaseLabel;
	private JLabel assignLabel;
	private JLabel firstnameLabel;
	private JTextField firstnameTextField;
	private JLabel lastnameLabel;
	private JTextField lastnameTextField;
	private JLabel mailLabel;
	private JTextField mailTextField;
	private JPanel saveCancelButtonPanel;
	private JButton saveButton;
	private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
