package fr.siocoliniere.view;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bll.MemberController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import net.miginfocom.swing.*;

public class WelcomeForm extends JFrame {

	private HackathonManagementForm viewHackathonManagement;
	private MemberManagementForm viewMemberManagement;
	private HackathonManagementSettingsForm viewHackathonManagementSettings;

	/**
	 * Constructeur
	 */
	public WelcomeForm() {

		setTitle("Hackat'Orga");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(500, 500);
		setPreferredSize(new Dimension(600, 400));

		initComponents();
		errorTextArea.setVisible(false);

		initComboBoxHackathons();
		initComboBoxMembers();

		this.viewHackathonManagement = new HackathonManagementForm();
		this.viewHackathonManagement.setVisible(false);

		this.viewMemberManagement = new MemberManagementForm();
		this.viewMemberManagement.setVisible(false);

		this.viewHackathonManagementSettings = new HackathonManagementSettingsForm(null);
		this.viewHackathonManagementSettings.setVisible(false);

	}

	/**
	 * Permet d'initialiser la liste déroulante avec les hackathons existants
	 */
	private void initComboBoxHackathons() {
		//!!! Info technique : un objet de type ComboBox fonctionne avec un objet de type DefaultComboBoxModel qui va contenir les données

		// récupération des hackathons
		List<Hackathon> hackathons = HackathonController.getInstance().getAll();

		// création d'un model à partir de la liste d'objets hackathons
		if (hackathons != null) {
			DefaultComboBoxModel<Hackathon> modelCombo = new DefaultComboBoxModel<>();
			for (Hackathon hacka : hackathons) {
				modelCombo.addElement(hacka);
			}
			//rattachement du DefaultComboBoxModel au composant graphique ComboBox
			hackathonsComboBox.setModel(modelCombo);

		} else {
			//gestion d'une erreur de récupération des données
			this.errorTextArea.setText("Problème de chargement de données");
			this.errorTextArea.setVisible(true);
			this.hackathonmanageButton.setEnabled(false);
		}
	}

	/**
	 * Permet d'initialiser la liste déroulante avec les membres existants
	 */
	private void initComboBoxMembers() {
		//!!! Info technique : un objet de type ComboBox fonctionne avec un objet de type DefaultComboBoxModel qui va contenir les données

		// récupération des membres
		List<Member> members = MemberController.getInstance().getAll();

		// création d'un model à partir de la liste d'objets membres
		if (members != null) {
			DefaultComboBoxModel<Member> modelCombo = new DefaultComboBoxModel<>();
			for (Member membre : members) {
				modelCombo.addElement(membre);
			}
			//rattachement du DefaultComboBoxModel au composant graphique ComboBox
			membersComboBox.setModel(modelCombo);

		} else {
			//gestion d'une erreur de récupération des données
			this.errorTextArea.setText("Problème de chargement de données");
			this.errorTextArea.setVisible(true);
			this.membermanageButton.setEnabled(false);
		}
	}


	/**
	 * METHODES RATTACHEES A LA GESTION DES EVENEMENTS
	 */

	/**
	 * Permet de quitter l'application
	 * Appelée : lors du clic sur le bouton Close
	 */
	private void closeButtonActionPerformed(ActionEvent e) {

		ConfirmForm confirmView = new ConfirmForm(this,"Confirmation", "Voulez-vous quitter ?");
		confirmView.pack();
		confirmView.setVisible(true);
		if (confirmView.isAnswer()) {
			// close confirmed
			System.exit(0);
		}
	}

	/**
	 * Permet de gérer le hackathon sélectionné
	 * Appelée : lors du clic sur le bouton Manage
	 */
	private void hackathonManageButtonActionPerformed(ActionEvent e) {

		//récupération du hackathon sélectionné
		Hackathon hackathonSelected = (Hackathon) hackathonsComboBox.getSelectedItem();

		//valorisation du hackathon sélectionné sur la vue permettant de gérer un hackathon et affichage de la vue
		this.viewHackathonManagement.setHackathon(hackathonSelected);
		this.viewHackathonManagement.setVisible(true);

	}
	private void memberManageButtonActionPerformed(ActionEvent e) {
		//récupération du hackathon sélectionné
		Member memberSelected = (Member) membersComboBox.getSelectedItem();
		//valorisation du membre sélectionné sur la vue permettant de gérer un membre et affichage de la vue
		this.viewMemberManagement.initMemberManaged(memberSelected);
		this.viewMemberManagement.setVisible(true);
	}

	/**
	 * Permet de gérer un nouvel hackathon
	 * Appelée : lors du clic sur le bouton New Hackathon
	 */
	private void newHackaButtonActionPerformed(ActionEvent e) {
		//valorisation d'un hackathon null sur la vue permettant de gérer un hackathon et affichage de la vue
		this.viewHackathonManagementSettings.initHackathonManaged(null);
		this.viewHackathonManagementSettings.setVisible(true);
	}

	/**
	 * Permet de gérer un nouveau membre
	 * Appelée : lors du clic sur le bouton New Member
	 */
	private void newMemberButtonActionPerformed(ActionEvent e) {
		this.viewMemberManagement.initMemberManaged(null);
		this.viewMemberManagement.setVisible(true);
	}

	private void thisWindowGainedFocus(WindowEvent e) {
		HackathonController.getInstance().update();
		initComboBoxHackathons();
		initComboBoxMembers();
	}

	/**
	 * JFormDesigner - DO NOT MODIFY
	 */

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		errorTextArea = new JLabel();
		hackathonLabel = new JLabel();
		hackathonsComboBox = new JComboBox();
		hackathonmanageButton = new JButton();
		newHackathonButton = new JButton();
		memberLabel = new JLabel();
		membersComboBox = new JComboBox();
		membermanageButton = new JButton();
		newMemberButton = new JButton();
		buttonBar = new JPanel();
		closeButton = new JButton();

		//======== this ========
		addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				thisWindowGainedFocus(e);
			}
		});
		var contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setLayout(new BorderLayout());

			//======== contentPanel ========
			{
				contentPanel.setBorder(null);
				contentPanel.setLayout(new MigLayout(
					"insets dialog,hidemode 3,align center center",
					// columns
					"[grow,fill]" +
					"[grow,fill]" +
					"[grow,fill]" +
					"[grow,fill]" +
					"[grow,fill]" +
					"[grow,fill]",
					// rows
					"[grow]" +
					"[grow]" +
					"[grow]" +
					"[grow]" +
					"[grow]" +
					"[grow]" +
					"[grow]" +
					"[grow]" +
					"[grow]"));

				//---- errorTextArea ----
				errorTextArea.setText("Message d'erreur");
				contentPanel.add(errorTextArea, "cell 1 0 4 1");

				//---- hackathonLabel ----
				hackathonLabel.setText("Hackathon : ");
				contentPanel.add(hackathonLabel, "cell 1 1 4 1");

				//---- hackathonsComboBox ----
				hackathonsComboBox.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				contentPanel.add(hackathonsComboBox, "cell 1 2 2 1");

				//---- hackathonmanageButton ----
				hackathonmanageButton.setText("G\u00e9rer");
				hackathonmanageButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				hackathonmanageButton.addActionListener(e -> hackathonManageButtonActionPerformed(e));
				contentPanel.add(hackathonmanageButton, "cell 3 2 2 1");

				//---- newHackathonButton ----
				newHackathonButton.setText("Nouvel Hackathon");
				newHackathonButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				newHackathonButton.addActionListener(e -> newHackaButtonActionPerformed(e));
				contentPanel.add(newHackathonButton, "cell 1 3 4 1");

				//---- memberLabel ----
				memberLabel.setText("Membre :");
				contentPanel.add(memberLabel, "cell 1 5 4 1");
				contentPanel.add(membersComboBox, "cell 1 6 2 1");

				//---- membermanageButton ----
				membermanageButton.setText("G\u00e9rer");
				membermanageButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				membermanageButton.addActionListener(e -> memberManageButtonActionPerformed(e));
				contentPanel.add(membermanageButton, "cell 3 6 2 1");

				//---- newMemberButton ----
				newMemberButton.setText("Nouveau Membre");
				newMemberButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				newMemberButton.addActionListener(e -> newMemberButtonActionPerformed(e));
				contentPanel.add(newMemberButton, "cell 1 7 4 1");
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			//======== buttonBar ========
			{
				buttonBar.setLayout(new MigLayout(
					"insets dialog,alignx right",
					// columns
					"[button,fill]",
					// rows
					null));

				//---- closeButton ----
				closeButton.setText("Fermer");
				closeButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				closeButton.addActionListener(e -> closeButtonActionPerformed(e));
				buttonBar.add(closeButton, "cell 0 0");
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JLabel errorTextArea;
	private JLabel hackathonLabel;
	private JComboBox hackathonsComboBox;
	private JButton hackathonmanageButton;
	private JButton newHackathonButton;
	private JLabel memberLabel;
	private JComboBox membersComboBox;
	private JButton membermanageButton;
	private JButton newMemberButton;
	private JPanel buttonBar;
	private JButton closeButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables


}
