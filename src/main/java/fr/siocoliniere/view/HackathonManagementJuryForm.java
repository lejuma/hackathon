/*
 * Created by JFormDesigner on Fri Sep 02 14:31:59 CEST 2022
 */

package fr.siocoliniere.view;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bll.MemberController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.*;
import java.util.List;

/**
 * @author unknown
 */
public class HackathonManagementJuryForm extends JDialog {
	private HackathonManagementCreateNewJuryForm viewHackathonManagementCreateNewJury;
	private Hackathon hackathonManaged;

	private void initComboBoxMembers() {
		//!!! Info technique : un objet de type ComboBox fonctionne avec un objet de type DefaultComboBoxModel qui va contenir les données

		// récupération des membres
		List<Member> members = MemberController.getInstance().getNonRoleMember(1);

		// création d'un model à partir de la liste d'objets hackathons
		if (members != null) {
			DefaultComboBoxModel<Member> modelCombo = new DefaultComboBoxModel<>();
			for (Member member : members) {
				modelCombo.addElement(member);
			}
			//rattachement du DefaultComboBoxModel au composant graphique ComboBox
			search_new_jury_combobox.setModel(modelCombo);

		} else {
			//gestion d'une erreur de récupération des données
			this.addExistingMemberButton.setEnabled(false);
		}
	}

	public void initHackathonManaged(Hackathon hackathonManaged) {
		this.hackathonManaged = hackathonManaged;

		if (this.hackathonManaged != null) {
			// initialisation de la JList des jurys existants
			initJuryList(hackathonManaged.getJuryMembers());
			initPotentialJuryList(HackathonController.getInstance().getPotentialJury(hackathonManaged.getId()));
		}
		else{
			initJuryList(new ArrayList<>());
			initPotentialJuryList(HackathonController.getInstance().getPotentialJury(0));
		}
	}

	public HackathonManagementJuryForm(Window owner) {
		super(owner);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setPreferredSize(new Dimension(750, 500));

		setTitle("Hackat'Orga : Jury ");

		initComponents();
		initComboBoxMembers();
		// instanciation des vues liées
		viewHackathonManagementCreateNewJury = new HackathonManagementCreateNewJuryForm(this);

		//masquage des vues liées
		viewHackathonManagementCreateNewJury.setVisible(false);

	}
	/**
	 * Permet de valoriser la Jlist des membres actuels du hackathon
	 * @param jurys liste des membres du jury
	 */
	private void initJuryList(java.util.List<Member> jurys) {
		DefaultListModel<Member> modelJuryList = new DefaultListModel<>();
		for (Member member : jurys) {
			modelJuryList.addElement(member);
		}
		//rattachement du DefaultListModel au composant graphique JList
		this.juryList.setModel(modelJuryList);
	}

	private void initPotentialJuryList(List<Member> memberList) {
		DefaultListModel<Member> modelPotentialJuryList = new DefaultListModel<>();
		for (Member member : memberList) {
			modelPotentialJuryList.addElement(member);
		}
		//rattachement du DefaultListModel au composant graphique JList
		this.potentialJuryList.setModel(modelPotentialJuryList);
	}

	private void removeButtonActionPerformed(ActionEvent e) {
		//récupération du membre jury sélectionné
		Member jurySelected = (Member) juryList.getSelectedValue();

		//suppression du membre sélectionné dans le model rattaché à la Jlist gérant les jurys du hackathon
		DefaultListModel<Member> modelJuryList = (DefaultListModel<Member>) juryList.getModel();
		modelJuryList.removeElement(jurySelected);

		//ajout du membre sélectionné au model rattaché à la JList gérant les membres jurys potentiels
		DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) potentialJuryList.getModel();
		modelMemberList.addElement(jurySelected);
	}
	private void addButtonActionPerformed(ActionEvent e) {

		//récupération du membre jury sélectionné
		Member jurySelected = (Member) potentialJuryList.getSelectedValue();

		//suppression du membre sélectionné dans le model rattaché à la Jlist gérant les jurys potentiels du hackathon
		DefaultListModel<Member> modelJuryList = (DefaultListModel<Member>) potentialJuryList.getModel();
		modelJuryList.removeElement(jurySelected);

		//ajout du membre sélectionné au model rattaché à la JList gérant les jurys
		DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) juryList.getModel();
		modelMemberList.addElement(jurySelected);
	}

	private void addNewButtonActionPerformed(ActionEvent e) {
		// Construction de la liste des jurys à sauvegarder
		List<Member> jurys = new ArrayList<>();
		for (int i =0; i< juryList.getModel().getSize(); i++){
			Member jury = (Member) juryList.getModel().getElementAt(i);
			jurys.add(jury);
		}

		//affichage de la vue pour le hackathon concerné
		viewHackathonManagementCreateNewJury.initHackathonManaged(hackathonManaged);
		viewHackathonManagementCreateNewJury.setVisible(true);
	}

	private void addJuryButtonActionPerformed(ActionEvent e) {
		//récupération du hackathon sélectionné
		Member memberManaged = (Member) search_new_jury_combobox.getSelectedItem();

        //suppression du membre sélectionné dans le model rattaché à la Jlist gérant les jurys potentiels du hackathon
        search_new_jury_combobox.removeItem(memberManaged);

        if (memberManaged != null){
            //ajout du membre sélectionné au model rattaché à la JList gérant les jurys
            DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) juryList.getModel();
            modelMemberList.addElement(memberManaged);
        }
	}

	private void SaveButtonActionPerformed(ActionEvent e) {
		// Construction de la liste des jurys à sauvegarder
		List<Member> jurys = new ArrayList<>();
		for (int i =0; i< juryList.getModel().getSize(); i++){
			Member jury = (Member) juryList.getModel().getElementAt(i);
            jurys.add(jury);
		}

        //persistence
		HackathonController.getInstance().saveParticipationJuryByIdHackathon(hackathonManaged,jurys);

        hackathonManaged.setJuryMembers(jurys);

        this.dispose();

	}
	private void cancelButtonActionPerformed(ActionEvent e) {
		this.dispose();
	}

	private void thisWindowGainedFocus(WindowEvent e) {
        HackathonController.getInstance().update();
        // initHackathonManaged(hackathonManaged);

        //ajout du membre tout juste créé au model rattaché à la JList gérant les jurys
        DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) juryList.getModel();
        modelMemberList.addElement(hackathonManaged.getTmpMember());
        hackathonManaged.setTmpMember(null);
    }

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		existingLabel = new JLabel();
		potentialLabel = new JLabel();
		juryMemberScrollPane = new JScrollPane();
		juryList = new JList();
		ARButtonPanel = new JPanel();
		addButton = new JButton();
		removeButton = new JButton();
		memberScrollPane = new JScrollPane();
		potentialJuryList = new JList();
		buttonBar = new JPanel();
		addUnJuryLabel = new JLabel();
		search_new_jury_combobox = new JComboBox();
		addExistingMemberButton = new JButton();
		createNewButton = new JButton();
		SaveButton = new JButton();
		cancelButton = new JButton();

		//======== this ========
		setPreferredSize(null);
		addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				thisWindowGainedFocus(e);
				thisWindowGainedFocus(e);
			}
		});
		var contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setLayout(new BorderLayout());

			//======== contentPanel ========
			{
				contentPanel.setLayout(new GridBagLayout());
				((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {35, 112, 0, 119, 0, 0};
				((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 239, 0};
				((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};
				((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

				//---- existingLabel ----
				existingLabel.setText("Jur\u00e9s s\u00e9lectionn\u00e9s");
				contentPanel.add(existingLabel, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- potentialLabel ----
				potentialLabel.setText("Jur\u00e9s potentiels");
				contentPanel.add(potentialLabel, new GridBagConstraints(3, 0, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
					new Insets(0, 0, 5, 0), 0, 0));

				//======== juryMemberScrollPane ========
				{
					juryMemberScrollPane.setViewportView(juryList);
				}
				contentPanel.add(juryMemberScrollPane, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(5, 5, 0, 10), 0, 0));

				//======== ARButtonPanel ========
				{
					ARButtonPanel.setLayout(new GridBagLayout());
					((GridBagLayout)ARButtonPanel.getLayout()).columnWidths = new int[] {0, 0};
					((GridBagLayout)ARButtonPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 42, 0};
					((GridBagLayout)ARButtonPanel.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
					((GridBagLayout)ARButtonPanel.getLayout()).rowWeights = new double[] {1.0, 0.0, 0.0, 1.0, 1.0, 1.0E-4};

					//---- addButton ----
					addButton.setIcon(new ImageIcon(getClass().getResource("/arrowG.png")));
					addButton.setText("Ajouter");
					addButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					addButton.addActionListener(e -> addButtonActionPerformed(e));
					ARButtonPanel.add(addButton, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
						GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
						new Insets(0, 0, 5, 0), 0, 0));

					//---- removeButton ----
					removeButton.setIcon(new ImageIcon(getClass().getResource("/arrowD.png")));
					removeButton.setText("Supprimer");
					removeButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					removeButton.addActionListener(e -> removeButtonActionPerformed(e));
					ARButtonPanel.add(removeButton, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
						GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
						new Insets(0, 0, 5, 0), 0, 0));
				}
				contentPanel.add(ARButtonPanel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//======== memberScrollPane ========
				{
					memberScrollPane.setViewportView(potentialJuryList);
				}
				contentPanel.add(memberScrollPane, new GridBagConstraints(3, 1, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			//======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {146, 58, 58, 66, 55};

				//---- addUnJuryLabel ----
				addUnJuryLabel.setText("Ajouter un membre en tant que jur\u00e9 :");
				buttonBar.add(addUnJuryLabel, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- search_new_jury_combobox ----
				search_new_jury_combobox.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				buttonBar.add(search_new_jury_combobox, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- addExistingMemberButton ----
				addExistingMemberButton.setText("Ajouter");
				addExistingMemberButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				addExistingMemberButton.addActionListener(e -> addJuryButtonActionPerformed(e));
				buttonBar.add(addExistingMemberButton, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- createNewButton ----
				createNewButton.setText("Cr\u00e9er un nouveau jur\u00e9");
				createNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				createNewButton.addActionListener(e -> addNewButtonActionPerformed(e));
				buttonBar.add(createNewButton, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- SaveButton ----
				SaveButton.setText("Sauvegarder");
				SaveButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				SaveButton.addActionListener(e -> SaveButtonActionPerformed(e));
				buttonBar.add(SaveButton, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- cancelButton ----
				cancelButton.setText("Annuler");
				cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
				buttonBar.add(cancelButton, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JLabel existingLabel;
	private JLabel potentialLabel;
	private JScrollPane juryMemberScrollPane;
	private JList juryList;
	private JPanel ARButtonPanel;
	private JButton addButton;
	private JButton removeButton;
	private JScrollPane memberScrollPane;
	private JList potentialJuryList;
	private JPanel buttonBar;
	private JLabel addUnJuryLabel;
	private JComboBox search_new_jury_combobox;
	private JButton addExistingMemberButton;
	private JButton createNewButton;
	private JButton SaveButton;
	private JButton cancelButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
