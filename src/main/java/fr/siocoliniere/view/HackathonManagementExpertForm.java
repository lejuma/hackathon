/*
 * Created by JFormDesigner on Fri Sep 02 14:31:59 CEST 2022
 */

package fr.siocoliniere.view;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bll.MemberController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.*;
import java.util.List;

/**
 * @author unknown
 */
public class HackathonManagementExpertForm extends JDialog {
    private HackathonManagementCreateNewExpertForm viewHackathonManagementCreateNewExpert;
    private Hackathon hackathonManaged;

    private void initComboBoxMembers() {
        //!!! Info technique : un objet de type ComboBox fonctionne avec un objet de type DefaultComboBoxModel qui va contenir les données

        // récupération des membres
        List<Member> members = MemberController.getInstance().getNonRoleMember(4);

        // création d'un model à partir de la liste d'objets hackathons
        if (members != null) {
            DefaultComboBoxModel<Member> modelCombo = new DefaultComboBoxModel<>();
            for (Member member : members) {
                modelCombo.addElement(member);
            }
            //rattachement du DefaultComboBoxModel au composant graphique ComboBox
            search_new_expert_combobox.setModel(modelCombo);

        } else {
            //gestion d'une erreur de récupération des données
            this.addExistingMemberButton.setEnabled(false);
        }
    }

    public void initHackathonManaged(Hackathon hackathonManaged) {
        this.hackathonManaged = hackathonManaged;

        if (this.hackathonManaged != null) {
            // initialisation de la JList des experts existants
            initExpeList(hackathonManaged.getExpertMembers());
            initPotentialExpeList(HackathonController.getInstance().getPotentialExpert(hackathonManaged.getId()));
        }
        else{
            initExpeList(new ArrayList<>());
            initPotentialExpeList(HackathonController.getInstance().getPotentialExpert(0));
        }
    }

    public HackathonManagementExpertForm(Window owner) {
        super(owner);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(750, 500));

        setTitle("Hackat'Orga : Expert ");

        initComponents();
        initComboBoxMembers();
        // instanciation des vues liées
        viewHackathonManagementCreateNewExpert = new HackathonManagementCreateNewExpertForm(this);

        //masquage des vues liées
        viewHackathonManagementCreateNewExpert.setVisible(false);

    }
    /**
     * Permet de valoriser la Jlist des membres actuels du hackathon
     * @param experts liste des membres expert
     */
    private void initExpeList(java.util.List<Member> experts) {
        DefaultListModel<Member> modelExpeList = new DefaultListModel<>();
        for (Member member : experts) {
            modelExpeList.addElement(member);
        }
        //rattachement du DefaultListModel au composant graphique JList
        this.expeList.setModel(modelExpeList);
    }

    private void initPotentialExpeList(List<Member> memberList) {
        DefaultListModel<Member> modelPotentialExpeList = new DefaultListModel<>();
        for (Member member : memberList) {
            modelPotentialExpeList.addElement(member);
        }
        //rattachement du DefaultListModel au composant graphique JList
        this.potentialExpeList.setModel(modelPotentialExpeList);
    }

    private void removeButtonActionPerformed(ActionEvent e) {
        //récupération de l'expert sélectionné
        Member expeSelected = (Member) expeList.getSelectedValue();

        //suppression du membre sélectionné dans le model rattaché à la Jlist gérant les experts du hackathon
        DefaultListModel<Member> modelExpeList = (DefaultListModel<Member>) expeList.getModel();
        modelExpeList.removeElement(expeSelected);

        //ajout du membre sélectionné au model rattaché à la JList gérant les membres experts potentiels
        DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) potentialExpeList.getModel();
        modelMemberList.addElement(expeSelected);
    }
    private void addButtonActionPerformed(ActionEvent e) {

        //récupération de l'expert sélectionné
        Member expeSelected = (Member) potentialExpeList.getSelectedValue();

        //suppression du membre sélectionné dans le model rattaché à la Jlist gérant les experts potentiels du hackathon
        DefaultListModel<Member> modelExpeList = (DefaultListModel<Member>) potentialExpeList.getModel();
        modelExpeList.removeElement(expeSelected);

        //ajout du membre sélectionné au model rattaché à la JList gérant les experts
        DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) expeList.getModel();
        modelMemberList.addElement(expeSelected);
    }

    private void addNewButtonActionPerformed(ActionEvent e) {
        // Construction de la liste des experts à sauvegarder
        List<Member> experts = new ArrayList<>();
        for (int i =0; i< expeList.getModel().getSize(); i++){
            Member expe = (Member) expeList.getModel().getElementAt(i);
            experts.add(expe);
        }

        //affichage de la vue pour le hackathon concerné
        viewHackathonManagementCreateNewExpert.initHackathonManaged(hackathonManaged);
        viewHackathonManagementCreateNewExpert.setVisible(true);
    }

    private void addExpeButtonActionPerformed(ActionEvent e) {
        //récupération du hackathon sélectionné
        Member memberManaged = (Member) search_new_expert_combobox.getSelectedItem();

        //suppression du membre sélectionné dans le model rattaché à la Jlist gérant les experts potentiels du hackathon
        search_new_expert_combobox.removeItem(memberManaged);

        if (memberManaged != null) {
            //ajout du membre sélectionné au model rattaché à la JList gérant les experts
            DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) expeList.getModel();
            modelMemberList.addElement(memberManaged);
        }
    }

    private void SaveButtonActionPerformed(ActionEvent e) {
        // Construction de la liste de expert à sauvegarder
        List<Member> experts = new ArrayList<>();
        for (int i =0; i< expeList.getModel().getSize(); i++){
            Member expe = (Member) expeList.getModel().getElementAt(i);
            experts.add(expe);
        }

        //persistence
        HackathonController.getInstance().saveParticipationExpertByIdHackathon(hackathonManaged,experts);

        hackathonManaged.setExpertMembers(experts);

        this.dispose();

    }
    private void cancelButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }

    private void thisWindowGainedFocus(WindowEvent e) {
        HackathonController.getInstance().update();
        // initHackathonManaged(hackathonManaged);

        //ajout du membre tout juste créé au model rattaché à la JList gérant les experts
        DefaultListModel<Member> modelMemberList = (DefaultListModel<Member>) expeList.getModel();
        modelMemberList.addElement(hackathonManaged.getTmpMember());
        hackathonManaged.setTmpMember(null);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		dialogPane = new JPanel();
		contentPanel = new JPanel();
		existingLabel = new JLabel();
		potentialLabel = new JLabel();
		expeMemberScrollPane = new JScrollPane();
		expeList = new JList();
		ARButtonPanel = new JPanel();
		addButton = new JButton();
		removeButton = new JButton();
		memberScrollPane = new JScrollPane();
		potentialExpeList = new JList();
		buttonBar = new JPanel();
		label1 = new JLabel();
		search_new_expert_combobox = new JComboBox();
		addExistingMemberButton = new JButton();
		createNewButton = new JButton();
		SaveButton = new JButton();
		cancelButton = new JButton();

		//======== this ========
		setMinimumSize(null);
		addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				thisWindowGainedFocus(e);
				thisWindowGainedFocus(e);
			}
		});
		var contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
			dialogPane.setLayout(new BorderLayout());

			//======== contentPanel ========
			{
				contentPanel.setLayout(new GridBagLayout());
				((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {35, 112, 0, 119, 0, 0};
				((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 239, 0};
				((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};
				((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

				//---- existingLabel ----
				existingLabel.setText("Experts s\u00e9lectionn\u00e9s");
				contentPanel.add(existingLabel, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- potentialLabel ----
				potentialLabel.setText(" Experts potentiels");
				contentPanel.add(potentialLabel, new GridBagConstraints(3, 0, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 0), 0, 0));

				//======== expeMemberScrollPane ========
				{
					expeMemberScrollPane.setViewportView(expeList);
				}
				contentPanel.add(expeMemberScrollPane, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(5, 5, 0, 10), 0, 0));

				//======== ARButtonPanel ========
				{
					ARButtonPanel.setLayout(new GridBagLayout());
					((GridBagLayout)ARButtonPanel.getLayout()).columnWidths = new int[] {0, 0};
					((GridBagLayout)ARButtonPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 42, 0};
					((GridBagLayout)ARButtonPanel.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
					((GridBagLayout)ARButtonPanel.getLayout()).rowWeights = new double[] {1.0, 0.0, 0.0, 1.0, 1.0, 1.0E-4};

					//---- addButton ----
					addButton.setIcon(new ImageIcon(getClass().getResource("/arrowG.png")));
					addButton.setText("Ajouter");
					addButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					addButton.addActionListener(e -> addButtonActionPerformed(e));
					ARButtonPanel.add(addButton, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
						GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
						new Insets(0, 0, 5, 0), 0, 0));

					//---- removeButton ----
					removeButton.setIcon(new ImageIcon(getClass().getResource("/arrowD.png")));
					removeButton.setText("Supprimer");
					removeButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					removeButton.addActionListener(e -> removeButtonActionPerformed(e));
					ARButtonPanel.add(removeButton, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
						GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
						new Insets(0, 0, 5, 0), 0, 0));
				}
				contentPanel.add(ARButtonPanel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//======== memberScrollPane ========
				{
					memberScrollPane.setViewportView(potentialExpeList);
				}
				contentPanel.add(memberScrollPane, new GridBagConstraints(3, 1, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(contentPanel, BorderLayout.CENTER);

			//======== buttonBar ========
			{
				buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
				buttonBar.setLayout(new GridBagLayout());
				((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {146, 58, 58, 66, 55};

				//---- label1 ----
				label1.setText("Ajouter un membre en tant qu'expert :");
				buttonBar.add(label1, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- search_new_expert_combobox ----
				search_new_expert_combobox.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				buttonBar.add(search_new_expert_combobox, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- addExistingMemberButton ----
				addExistingMemberButton.setText("Ajouter");
				addExistingMemberButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				addExistingMemberButton.addActionListener(e -> addExpeButtonActionPerformed(e));
				buttonBar.add(addExistingMemberButton, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 5, 5), 0, 0));

				//---- createNewButton ----
				createNewButton.setText("Cr\u00e9er un nouvel expert");
				createNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				createNewButton.addActionListener(e -> addNewButtonActionPerformed(e));
				buttonBar.add(createNewButton, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- SaveButton ----
				SaveButton.setText("Sauvegarder");
				SaveButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				SaveButton.addActionListener(e -> SaveButtonActionPerformed(e));
				buttonBar.add(SaveButton, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 5), 0, 0));

				//---- cancelButton ----
				cancelButton.setText("Annuler");
				cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
				buttonBar.add(cancelButton, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane.add(dialogPane, BorderLayout.CENTER);
		pack();
		setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel dialogPane;
	private JPanel contentPanel;
	private JLabel existingLabel;
	private JLabel potentialLabel;
	private JScrollPane expeMemberScrollPane;
	private JList expeList;
	private JPanel ARButtonPanel;
	private JButton addButton;
	private JButton removeButton;
	private JScrollPane memberScrollPane;
	private JList potentialExpeList;
	private JPanel buttonBar;
	private JLabel label1;
	private JComboBox search_new_expert_combobox;
	private JButton addExistingMemberButton;
	private JButton createNewButton;
	private JButton SaveButton;
	private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
