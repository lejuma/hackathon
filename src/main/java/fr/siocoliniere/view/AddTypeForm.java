/*
 * Created by JFormDesigner on Tue Nov 08 15:00:11 CET 2022
 */

package fr.siocoliniere.view;

import fr.siocoliniere.bll.LieuController;
import fr.siocoliniere.bll.TypeInscriptionController;
import fr.siocoliniere.bo.Lieu;
import fr.siocoliniere.bo.TypeInscription;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * @author unknown
 */
public class AddTypeForm extends JDialog {
	private TypeInscription typeManaged;
	public AddTypeForm(Window owner) {
		super(owner);
		setPreferredSize(new Dimension(500, 80));
		initComponents();
	}

	/**
	 * Permet d'initialiser la vue pour un type d'inscription choisi
	 * @param typeManaged
	 */
	public void initLieuManaged(TypeInscription typeManaged) {
		this.typeManaged = typeManaged;

	}

	private void thisWindowOpened(WindowEvent e) {
		this.nameTextField.setText("");
	}

	private void cancelButtonActionPerformed(ActionEvent e) {
		this.dispose();
	}

	private void addButtonActionPerformed(ActionEvent e) {
		if (!(nameTextField.getText().isBlank())) {
			TypeInscriptionController.getInstance().saveTypeInscriptionStandalone(typeManaged, nameTextField.getText());
			this.dispose();
		}
	}


	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
		nameLabel = new JLabel();
		nameTextField = new JTextField();
		addButton = new JButton();
		cancelButton = new JButton();

		//======== this ========
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				thisWindowOpened(e);
			}
		});
		var contentPane = getContentPane();
		contentPane.setLayout(new GridBagLayout());
		((GridBagLayout)contentPane.getLayout()).columnWidths = new int[] {0, 147, 0, 205, 0, 0};
		((GridBagLayout)contentPane.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
		((GridBagLayout)contentPane.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4};
		((GridBagLayout)contentPane.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

		//---- nameLabel ----
		nameLabel.setText("Nom du type d'inscription*");
		nameLabel.setPreferredSize(null);
		contentPane.add(nameLabel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
			GridBagConstraints.EAST, GridBagConstraints.NONE,
			new Insets(0, 0, 5, 5), 0, 0));
		contentPane.add(nameTextField, new GridBagConstraints(2, 1, 2, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, 5, 5), 0, 0));

		//---- addButton ----
		addButton.setText("Ajouter");
		addButton.addActionListener(e -> addButtonActionPerformed(e));
		contentPane.add(addButton, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 5, 5), 0, 0));

		//---- cancelButton ----
		cancelButton.setText("Annuler");
		cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
		contentPane.add(cancelButton, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.NONE,
			new Insets(0, 0, 5, 5), 0, 0));
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
	private JLabel nameLabel;
	private JTextField nameTextField;
	private JButton addButton;
	private JButton cancelButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
