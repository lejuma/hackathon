package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOMember;

import java.util.List;

public class MemberController {

	/**
	 * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
	 */
	private static MemberController instanceCtrl;
    private List<Member> members;

	/**
	 * Pattern Singleton
	 * @return MemberController
	 */
	public static synchronized MemberController getInstance() {
		if(instanceCtrl == null){
			instanceCtrl = new MemberController();
		}
		return instanceCtrl;
	}

    /**
     * CONSTRUCTEUR
     */
    private MemberController() {
        try {
            this.members = DAOMember.getInstance().getAll();
        } catch (DALException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de récupérer l'ensemble des membres ayant été jury
     * @return la liste des membres
     */
    public List<Member> getAll() {
        List<Member> members = null;
        try {
            members = DAOMember.getInstance().getAll();
        } catch (DALException e) {
            e.printStackTrace();
        }
        return members;
    }


    /**
     * Permet de récupérer l'ensemble des membres ayant été jury
     * @return la liste des membres
     */
    public List<Member> getAllByRoleJury() {
        List<Member> jurys = null;
        try {
            jurys = DAOMember.getInstance().getAllJuryMember();
        } catch (DALException e) {
            e.printStackTrace();
        }
        return jurys;
    }

    /**
     * Permet de récupérer l'ensemble des membres ayant été animateur
     * @return la liste des membres
     */
    public List<Member> getAllByRoleAnim() {
        List<Member> animators = null;
        try {
            animators = DAOMember.getInstance().getAllAnimMember();
        } catch (DALException e) {
            e.printStackTrace();
        }
        return animators;
    }

    /**
     * Permet de récupérer l'ensemble des membres ayant été expert
     * @return la liste des membres
     */
    public List<Member> getAllByRoleExpert() {
        List<Member> experts = null;
        try {
            experts = DAOMember.getInstance().getAllExpertMember();
        } catch (DALException e) {
            e.printStackTrace();
        }
        return experts;
    }

    public List<Member> getNonRoleMember(int idrole) {
        List<Member> nonRoleMembers = null;
        try {
            nonRoleMembers = DAOMember.getInstance().getNonRoleMembersOnly(3);
        } catch (DALException e) {
            e.printStackTrace();
        }
        return nonRoleMembers;
    }

    public void saveMemberStandalone(Member memberManaged, String firstname, String lastname, String mail) {
        if (memberManaged != null) {
            memberManaged.setFirstname(firstname);
            memberManaged.setLastname(lastname);
            memberManaged.setMail(mail);
            //persistance : Update
            try {
                DAOMember.getInstance().update(memberManaged);
            } catch (DALException e) {
                e.printStackTrace();
            }
        } else {
            // Nouveau membre
            memberManaged = new Member(firstname, lastname, mail);
            //persistance : Insert
            try {
                DAOMember.getInstance().saveMember(memberManaged);
            } catch (DALException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Member> getAllByRoleAnimFromOtherHackathon(int hackid) {
        List<Member> animators = null;
        try {
            animators = DAOMember.getInstance().getOnlyAnimFromOtherHackathon(hackid);
        } catch (DALException e) {
            e.printStackTrace();
        }
        return animators;
    }

    public int getMemberIdByName(String firstname, String lastname, String mail) {
        int idmem = 0;
        try {
            idmem = DAOMember.getInstance().getIdByFirstLastName(firstname, lastname, mail);
        } catch (DALException e) {
            e.printStackTrace();
        }
        return idmem;
    }

    public List<Member> getAllByRoleJuryFromOtherHackathons(int idhack) {
        List<Member> jurys = null;
        try {
            jurys = DAOMember.getInstance().getAllJuryMemberFromOtherHackathonsOnly(idhack);
        } catch (DALException e) {
            e.printStackTrace();
        }
        return jurys;
    }

    public List<Member> getAllByRoleExpertFromOtherHackathon(int hackid) {
        List<Member> experts = null;
        try {
            experts = DAOMember.getInstance().getOnlyExpertFromOtherHackathon(hackid);
        } catch (DALException e) {
            e.printStackTrace();
        }
        return experts;
    }
}
