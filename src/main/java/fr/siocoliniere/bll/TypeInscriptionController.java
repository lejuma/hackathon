package fr.siocoliniere.bll;

import fr.siocoliniere.bo.TypeInscription;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOTypeInscription;

import java.util.List;

public class TypeInscriptionController {
	private List<TypeInscription> lesTyesInscr;
	private int idTypeInscr;

	/**
	 * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
	 */
	private static TypeInscriptionController instanceCtrl;

	/**
	 * Pattern Singleton
	 * @return LieuController
	 */
	public static synchronized TypeInscriptionController getInstance(){
		if(instanceCtrl == null){
			instanceCtrl = new TypeInscriptionController();
		}
		return instanceCtrl;
	}

	/**
	 * Constructeur
	 * Chargement de la liste des lieux
	 * En private : pattern Singleton
	 */
	private TypeInscriptionController() {

		try {
			this.lesTyesInscr = DAOTypeInscription.getInstance().getAll();
		} catch (DALException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Permet de récupérer l'ensemble des types d'inscriptions
	 * @return la liste des types d'inscriptions
	 */
	public List<TypeInscription> getAll(){
		return lesTyesInscr;
	}

	public int getIdByName(String nom) {
		try {
			idTypeInscr = DAOTypeInscription.getInstance().getIdByName(nom);
		} catch (DALException e) {
			e.printStackTrace();
		}
		return idTypeInscr;
	}


	/**
	 * Permet de sauvegarder les settings d'un type d'inscription
	 * @param typeManaged
	 * @param nom nom du type d'inscription
	 */
	public void saveTypeInscriptionStandalone(TypeInscription typeManaged, String nom) {

		if (typeManaged != null) {
			// MàJ lieu existant
			typeManaged.setName(nom);
			//persistance : Update
			try {
				DAOTypeInscription.getInstance().updateSettings(typeManaged);
			} catch (DALException e) {
				e.printStackTrace();
			}
		} else {
			// Nouveau lieu
			typeManaged = new TypeInscription(nom);
			//persistance : Insert
			try {
				DAOTypeInscription.getInstance().saveTypeInscription(typeManaged);
			} catch (DALException e) {
				e.printStackTrace();
			}
		}
	}


	public void deleteTypeInscription(TypeInscription typeManaged){
		try {
			lesTyesInscr.remove(DAOTypeInscription.getInstance().getOneById(typeManaged.getId()));

			//suppression des types d'inscription en fonction de l'id
			DAOTypeInscription.getInstance().deleteTypeInscriptionById(typeManaged.getId());
		} catch (DALException e) {
			e.printStackTrace();
		}

	}


	public void update(){
		try {
			this.lesTyesInscr = DAOTypeInscription.getInstance().getAll();
		} catch (DALException e) {
			e.printStackTrace();
		}
	}
}
