package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Phase;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOPhase;

import java.util.List;

public class PhaseController {
	private List<Phase> lesPhases;
	private int idPhase;

	/**
	 * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
	 */
	private static PhaseController instanceCtrl;

	/**
	 * Pattern Singleton
	 * @return PhaseController
	 */
	public static synchronized PhaseController getInstance(){
		if(instanceCtrl == null) {
			instanceCtrl = new PhaseController();
		}
		return instanceCtrl;
	}

	/**
	 * Constructeur
	 * Chargement de la liste des phases
	 * En private : pattern Singleton
	 */
	private PhaseController() {

		try {
			this.lesPhases = DAOPhase.getInstance().getAll();
		} catch (DALException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Permet de récupérer l'ensemble des phases
	 * @return la liste des phases
	 */
	public List<Phase> getAll(){
		return lesPhases;
	}

	public int getIdByName(String nom) {
		int idPhase = 0;
		try {
			idPhase = DAOPhase.getInstance().getIdByName(nom);
		} catch (DALException e) {
			e.printStackTrace();
		}
		return idPhase;
	}

	public Phase getOneById(int id) {
		Phase unePhase = null;
		try {
			unePhase = DAOPhase.getInstance().getOneById(id);
		} catch (DALException e) {
			e.printStackTrace();
		}
		return unePhase;
	}



	/**
	 * Permet de sauvegarder les settings d'un phase
	 * @param phaseManaged
	 * @param nom nom de la phase
	 */
	public void savePhaseStandalone(Phase phaseManaged, String nom) {

		if (phaseManaged != null) {
			// MàJ phase existant
			phaseManaged.setName(nom);
			//persistance : Update
			try {
				DAOPhase.getInstance().updateSettings(phaseManaged);
			} catch (DALException e) {
				e.printStackTrace();
			}
		} else {
			// Nouveau phase
			phaseManaged = new Phase(nom);
			//persistance : Insert
			try {
				DAOPhase.getInstance().savePhase(phaseManaged);
			} catch (DALException e) {
				e.printStackTrace();
			}
		}
	}


	public void deletePhase(Phase phaseManaged){
		try {
			lesPhases.remove(DAOPhase.getInstance().getOneById(phaseManaged.getId()));

			//suppression des phases en fonction de l'id
			DAOPhase.getInstance().deletePhaseById(phaseManaged.getId());
		} catch (DALException e) {
			e.printStackTrace();
		}

	}


	public void update(){
		try {
			this.lesPhases = DAOPhase.getInstance().getAll();
		} catch (DALException e) {
			e.printStackTrace();
		}
	}
}
