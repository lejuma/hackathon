package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Lieu;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOLieu;

import java.util.List;

public class LieuController {
	private List<Lieu> lesLieux;
	private int idLieu;

	/**
	 * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
	 */
	private static LieuController instanceCtrl;

	/**
	 * Pattern Singleton
	 * @return LieuController
	 */
	public static synchronized LieuController getInstance(){
		if(instanceCtrl == null) {
			instanceCtrl = new LieuController();
		}
		return instanceCtrl;
	}

	/**
	 * Constructeur
	 * Chargement de la liste des lieux
	 * En private : pattern Singleton
	 */
	private LieuController() {

		try {
			this.lesLieux = DAOLieu.getInstance().getAll();
		} catch (DALException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Permet de récupérer l'ensemble des lieux
	 * @return la liste des lieux
	 */
	public List<Lieu> getAll(){
		return lesLieux;
	}

	public int getIdByName(String nom, String ville) {
		try {
			idLieu = DAOLieu.getInstance().getIdByName(nom, ville);
		} catch (DALException e) {
			e.printStackTrace();
		}
		return idLieu;
	}



	/**
	 * Permet de sauvegarder les settings d'un lieu
	 * @param lieuManaged
	 * @param nom nom du lieu
	 */
	public void saveLieuStandalone(Lieu lieuManaged, String nom, String ville) {

		if (lieuManaged != null) {
			// MàJ lieu existant
			lieuManaged.setNom(nom);
			lieuManaged.setVille(ville);
			//persistance : Update
			try {
				DAOLieu.getInstance().updateSettings(lieuManaged);
			} catch (DALException e) {
				e.printStackTrace();
			}
		} else {
			// Nouveau lieu
			lieuManaged = new Lieu(nom, ville);
			//persistance : Insert
			try {
				DAOLieu.getInstance().saveLieu(lieuManaged);
			} catch (DALException e) {
				e.printStackTrace();
			}
		}
	}


	public void deleteLieu(Lieu lieuManaged){
		try {
			lesLieux.remove(DAOLieu.getInstance().getOneById(lieuManaged.getId()));

			//suppression des lieux en fonction de l'id
			DAOLieu.getInstance().deleteLieuById(lieuManaged.getId());
		} catch (DALException e) {
			e.printStackTrace();
		}

	}


	public void update(){
		try {
			this.lesLieux = DAOLieu.getInstance().getAll();
		} catch (DALException e) {
			e.printStackTrace();
		}
	}
}
