package fr.siocoliniere.bll;

import fr.siocoliniere.bo.*;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOHackathon;
import fr.siocoliniere.dal.jdbc.DAOMember;

import java.sql.Date;
import java.util.List;

public class HackathonController {
	private List<Hackathon> hackathons;

	/**
	 * PATTERN SINGLETON : contraint l'instanciation d'une UNIQUE instance de classe
	 */
	private static HackathonController instanceCtrl;

	/**
	 * Pattern Singleton
	 * @return HackathonController
	 */
	public static synchronized HackathonController getInstance(){
		if(instanceCtrl == null){
			instanceCtrl = new HackathonController();
		}
		return instanceCtrl;
	}

	/**
	 * Constructeur
	 * Chargement de la liste des hackathons
	 * En private : pattern Singleton
	 */
	private HackathonController() {

		try {
			this.hackathons = DAOHackathon.getInstance().getAll();
		} catch (DALException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Permet de récupérer l'ensemble des hackathons
	 * @return la liste des hackathons
	 */
	public List<Hackathon> getAll(){
		return hackathons;
	}

	public List<TypeInscription> getAllTypeInscription() {
		return TypeInscriptionController.getInstance().getAll();
	}
	public int getIdTypeInscriptionByName(String name) {
		return TypeInscriptionController.getInstance().getIdByName(name);
	}



	public List<Lieu> getAllLieu() {
		return LieuController.getInstance().getAll();
	}
	public int getIdLieuByNom(String nom, String ville) {
		return LieuController.getInstance().getIdByName(nom, ville);
	}



	public List<Phase> getAllPhase() {
		return PhaseController.getInstance().getAll();
	}
	public int getIdPhaseByNom(String nom) {
		return PhaseController.getInstance().getIdByName(nom);
	}


	/**
	 * work in progress
	 */

    public int getOneMemberIDByName(String firstname, String lastname, String mail) {
        return MemberController.getInstance().getMemberIdByName(firstname, lastname, mail);
    }

    public List<Member> getPotentialJury(int idhack) {
        return MemberController.getInstance().getAllByRoleJuryFromOtherHackathons(idhack);
    }

	public List<Member> getPotentialAnim(int hackid) {
		return MemberController.getInstance().getAllByRoleAnimFromOtherHackathon(hackid);
	}

    public List<Member> getPotentialExpert(int hackid) {
        return MemberController.getInstance().getAllByRoleExpertFromOtherHackathon(hackid);
    }

    /**
     * Permet de sauvegarder les settings d'un hackathon
     * @param hackathonManaged
     * @param name nom du hacakthon
     * @param topic topic du hackathon
     * @param description description du hackathon
     * @param periodBegin date de début du hackathon
     * @param periodEnd date de fin du hackathon
	 * @param idLieu id du lieu du l'hackathon
	 * @param typeInscriptionId id du type d'inscription du hackathon
	 * @param idPhase id de la phase du hackathon
     */
    public void saveHackathonStandalone(Hackathon hackathonManaged, String name, String topic, String description, Date periodBegin, Date periodEnd, int idLieu, int typeInscriptionId, int idPhase) {

		if (hackathonManaged != null) {
			// MAJ hackathon existant
			hackathonManaged.setName(name);
			hackathonManaged.setTopic(topic);
			hackathonManaged.setDescription(description);
			hackathonManaged.setPeriodBegin(periodBegin);
			hackathonManaged.setPeriodEnd(periodEnd);
			hackathonManaged.setIdLieu(idLieu);
			hackathonManaged.setTypeInscriptionId(typeInscriptionId);
			hackathonManaged.setIdPhase(idPhase);
			//persistance : Update
			try {
				DAOHackathon.getInstance().updateSettings(hackathonManaged);
			} catch (DALException e) {
				e.printStackTrace();
			}
		} else {
			// Nouvel hackathon
			hackathonManaged = new Hackathon(name, topic, description, periodBegin, periodEnd, idLieu, typeInscriptionId, idPhase);
			//persistance : Insert
			try {
				DAOHackathon.getInstance().saveHackathon(hackathonManaged);
			} catch (DALException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Permet de persister l'ensemble des membres jouant le rôle de jury du hackathon
	 * @param hackathonManaged : hackathon
	 * @param jurys : liste de membres
	 */
	public void saveParticipationJuryByIdHackathon(Hackathon hackathonManaged, List<Member> jurys) {

		// maj objet
		hackathonManaged.clearJuryMember();
		hackathonManaged.setJuryMembers(jurys);

		//persistance
		try {
			//suppression des participations de membres en tant que jurys stockées en BDD
			DAOHackathon.getInstance().deleteAllParticipationJuryOfHackathon(hackathonManaged.getId());

			// insertion des nouveaux jurys
			for (Member member :jurys) {
				if (member != null) {
					DAOHackathon.getInstance().saveParticipationRoleJury(hackathonManaged.getId(), member.getId());
				}
			}
		} catch (DALException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Permet de persister l'ensemble des membres jouant le rôle d'animateur du hackathon
	 * @param hackathonManaged : hackathon
	 * @param animators : liste de membres
	 */
	public void saveParticipationAnimByIdHackathon(Hackathon hackathonManaged, List<Member> animators) {

		// maj objet
		hackathonManaged.clearAnimMember();
		hackathonManaged.setAnimMembers(animators);

		//persistance
		try {
			//suppression des participations de membres en tant qu'animateur stockées en BDD
			DAOHackathon.getInstance().deleteAllParticipationAnimOfHackathon(hackathonManaged.getId());

			// insertion des nouveaux animateurs
			for (Member member :animators) {
				if (member != null) {
					DAOHackathon.getInstance().saveParticipationRoleAnim(hackathonManaged.getId(), member.getId());
				}
			}

		} catch (DALException e) {
			e.printStackTrace();
		}

	}


	/**
	 * Permet de persister l'ensemble des membres jouant le rôle d'expert du hackathon
	 * @param hackathonManaged : hackathon
	 * @param experts : liste de membres
	 */

    public void saveParticipationExpertByIdHackathon(Hackathon hackathonManaged, List<Member> experts) {

        // maj objet
        hackathonManaged.clearExpertMember();
        hackathonManaged.setExpertMembers(experts);

        //persistance
        try {
            //suppression des participations de membres en tant qu'experts stockées en BDD
            DAOHackathon.getInstance().deleteAllParticipationExpertOfHackathon(hackathonManaged.getId());

            // insertion des nouveaux experts
            for (Member member :experts) {
				if (member != null) {
					DAOHackathon.getInstance().saveParticipationRoleExpert(hackathonManaged.getId(), member.getId());
				}
			}
        } catch (DALException e) {
            e.printStackTrace();
        }
    }




	public void saveMember(String firstname, String lastname, String mail) {
		try {
			Member member = new Member(firstname, lastname, mail);
			DAOMember.getInstance().saveMember(member);
		} catch (DALException e) {
			e.printStackTrace();
		}
	}
	public void saveParticipationAnimateur(int idhack, int idmem) {
		try {
			DAOHackathon.getInstance().saveParticipationRoleAnim(idhack, idmem);
		} catch (DALException e) {
			e.printStackTrace();
		}
	}
	public void saveParticipationJury(int idhack, int idmem) {
		try {
			DAOHackathon.getInstance().saveParticipationRoleJury(idhack, idmem);
		} catch (DALException e) {
			e.printStackTrace();
		}
	}
	public void saveParticipationExpert(int idhack, int idmem) {
		try {
			DAOHackathon.getInstance().saveParticipationRoleExpert(idhack, idmem);
		} catch (DALException e) {
			e.printStackTrace();
		}
	}
    public void deleteHackathon(Hackathon hackathonManaged){
        try {
            hackathons.remove(DAOHackathon.getInstance().getOneById(hackathonManaged.getId()));

            //suppression des participations en fonction de l'ID Hackathon
            DAOHackathon.getInstance().deleteParticipationByIdHackathon(hackathonManaged.getId());

            //suppression des hackathon en fonction de l'ID Hackathon
            DAOHackathon.getInstance().deleteHackathonById(hackathonManaged.getId());
        } catch (DALException e) {
            e.printStackTrace();
        }

    }

    public void update(){
        try {
            this.hackathons = DAOHackathon.getInstance().getAll();
        } catch (DALException e) {
            e.printStackTrace();
        }
    }
}
