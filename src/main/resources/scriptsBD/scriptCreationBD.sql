drop table member cascade;
create table member
(
    id            serial  not null,
    firstname     varchar not null,
    lastname      varchar not null,
    mail          varchar,
    telephone     varchar,
    datenaissance varchar,
    lienportfolio varchar,


    constraint member_pkey primary key (id)
);



drop table utilisateur cascade;
create table utilisateur
(
    id       serial not null,
    idmember int    not null,
    username text   not null unique,
    password text   not null,

    constraint utilisateur_pkey primary key (id)
);



drop table phase cascade;
create table phase
(
    id   serial not null,
    name text   not null,

    constraint phase_pkey primary key (id)
);



drop table role cascade;
create table role
(
    id   serial  not null,
    name varchar not null,

    constraint role_pkey primary key (id)

);



drop table typeinscription cascade;
create table typeinscription
(
    id   serial      not null,
    name varchar(30) not null,

    constraint type_inscription_pkey primary key (id)
);



drop table lieu cascade;
create table lieu
(
    id    serial  not null,
    nom   varchar not null,
    ville varchar not null,

    constraint lieu_pkey primary key (id)
);



drop table hackathon cascade;
create table hackathon
(
    id                serial      not null,
    name              varchar(50) not null unique,
    topic             text        not null,
    description       text        not null,
    periodbegin       date        not null,
    periodend         date        not null,
    idlieu            int         not null,
    typeinscriptionid int         not null,
    idphase           int         not null default 1,

    constraint hackathon_pkey primary key (id)
);



drop table projet cascade;
create table projet
(
    id              serial       not null,
    idhackathon     int          not null,
    nom             varchar(50)  not null,
    description     varchar(100) not null,
    est_selectionne bool         not null default false,

    constraint projet_pkey primary key (id, idhackathon)
);



drop table equipe cascade;
create table equipe
(
    id          serial  not null,
    idprojet    int     not null,
    idhackathon int     not null,
    nom         varchar not null,
    idchef      int,

    constraint equipe_pkey primary key (id, idprojet, idhackathon)
);



drop table participation cascade;
create table participation
(
    id          serial  not null,
    hackathonid integer not null,
    memberid    integer not null,
    roleid      integer not null,
    projetid    integer,
    equipeid    integer,
    competence  text, --texte libre sur les compétences en lien avec le thème du Hackathon
    estAccepte  boolean default true,
    motifRefus  text,

    constraint participation_pkey primary key (id),
    unique (hackathonid, memberid)
);



drop table voteprojet cascade;
create table voteprojet
(
    idexpert    int not null,
    idprojet    int not null,
    idhackathon int not null,
    score       int not null,

    constraint voteprojet_pkey primary key (idexpert, idprojet)
);



alter table utilisateur
    add constraint utilisateur_fk_member foreign key (idmember) references member (id);


alter table participation
    add constraint participation_fk_member foreign key (memberid) references member (id),
    add constraint participation_fk_hackathon foreign key (hackathonid) references hackathon (id),
    add constraint participation_fk_role foreign key (roleid) references role (id),
    add constraint participation_fk_equipe foreign key (equipeid, projetid, hackathonid) references equipe (id, idprojet, idhackathon);


alter table hackathon
    add constraint hackathon_fk_typeinscription foreign key (typeinscriptionid) references typeinscription (id),
    add constraint hackathon_fk_lieu foreign key (idlieu) references lieu (id),
    add constraint hackathon_fk_phase foreign key (idphase) references phase (id);

alter table projet
    add constraint projet_fk_hackathon foreign key (idhackathon) references hackathon (id);


alter table equipe
    add constraint equipe_fk_projet foreign key (idprojet, idhackathon) references projet (id, idhackathon),
    add constraint equipe_fk_participation foreign key (idchef) references participation (id);


alter table voteprojet
    add constraint voteprojet_fk_projet foreign key (idprojet, idhackathon) references projet (id, idhackathon),
    add constraint voteprojet_fk_participation foreign key (idexpert) references participation (id); /*uniquement les participation ayant le role expert*/

