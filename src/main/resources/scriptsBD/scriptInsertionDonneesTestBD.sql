insert into typeInscription(name)
values ('Libre'),
       ('Automatique'),
       ('Soumise à approbation');



insert into phase (name)
values ('A venir'),
       ('Inscription'),
       ('Votes des experts'),
       ('Sélection des projets'),
       ('Positionnement des participants sur les projets'),
       ('Constitution des équipes'),
       ('Production'),
       ('Terminé');



insert into role(name)
values ('Jury'),
       ('Inscrit'),
       ('Animateur'),
       ('Expert'),
       ('Organisateur');



insert into lieu(nom, ville)
values ('Cité des Congrès', 'Nantes'),
       ('Zénith', 'Nantes');



insert into member(firstname, lastname)
values ('Mathieu', 'Grisse'),
       ('Sara', 'Brucker'),
       ('Florence', 'Markus'),
       ('Théo', 'Lopez'),
       ('Thomas', 'Pasque'),
       ('Marc', 'Tombour'),
       ('Bryan', 'Wouèriz'),
       ('Cindy', 'Akbou'),
       ('Mathilde', 'Antas'),
       ('Teddy', 'Martin'),
       ('Julien', 'Lourau'),
       ('Charlie', 'Haden'),
       ('Cindy', 'Lopez'),
       ('Jean-Marc', 'Généreux'),
       ('Harry', 'Potter'),
       ('Charlie', 'Elachock--Ollatery'),
       ('Airde', 'Dédeux'),
       ('Elric', 'Edward'),
       ('Admin', 'Jury'),
       ('Admin', 'Inscrit'),
       ('Admin', 'Animateur'),
       ('Admin', 'Expert'),
       ('Admin', 'Organisateur');



insert into utilisateur(idmember, username, password)
values (19, 'jury', 'cbabe5c78b280e3c1a6303aae8589ddff01ea1f7a87f795e4e26811d05f90c3e'),
       (20, 'inscrit', 'cbabe5c78b280e3c1a6303aae8589ddff01ea1f7a87f795e4e26811d05f90c3e'),
       (21, 'anim', 'cbabe5c78b280e3c1a6303aae8589ddff01ea1f7a87f795e4e26811d05f90c3e'),
       (22, 'expert', 'cbabe5c78b280e3c1a6303aae8589ddff01ea1f7a87f795e4e26811d05f90c3e'),
       (23, 'orga', 'cbabe5c78b280e3c1a6303aae8589ddff01ea1f7a87f795e4e26811d05f90c3e');



insert into hackathon(name, topic, description, periodBegin, periodEnd, idLieu, typeInscriptionId, idphase)
values ('HackAct', 'Citoyenneté Mondiale', 'Ce hackathon sur la Citoyenneté Mondiale, ouverts aux jeunes,
        doit permettre d’échanger autour de projets concrets répondant aux objectifs de développement durable de l’ONU.',
        '2023-04-21', '2023-04-22', 1, 1, 1),
       ('HackInnov', 'Services publics et Innovation',
        'Ce hackathon vise à réunir les citoyens d’une communauté pour développer des solutions afin d’améliorer les services publics offerts aux citoyens',
        '2023-04-23', '2023-04-24', 2, 1, 2),
       ('HackSante', 'Services hospitaliers',
        'Ce hackathon vise à développer des solutions innovantes pour réinventer les services hospitaliers et notamment le service des urgences dans un cadre festif de fin d année',
        '2022-12-30', '2023-01-02', 1, 2, 5),
       ('HackConsul', 'Réinventer un métier',
        'Ce hackathon vise à réunir des étudiants afin de réinventer le métier de Consultant', '2023-02-24',
        '2023-04-25', 2, 3, 1);



insert into projet(nom, idhackathon, description, est_selectionne)
values ('ONU.mouv', 1, 'Ceci est une description', false),
       ('siteJDC', 1, 'Ceci est une description', false);



insert into equipe(nom, idprojet, idhackathon)
values ('Alpha', 1, 1),
       ('Beta', 2, 1);


insert into participation(hackathonid, memberid, roleid, equipeid)
values (1, 1, 1, 1),
       (1, 2, 1, 2),
       (1, 3, 1, 1),
       (2, 5, 1, 1),
       (2, 6, 1, null),
       (3, 7, 1, 1),
       (3, 8, 1, 1),
       (1, 9, 2, null),
       (1, 10, 2, null),
       (1, 11, 1, 2),
       (1, 12, 2, null),
       (1, 13, 3, null),
       (2, 14, 3, null),
       (2, 15, 3, null),
       (2, 10, 3, null),
       (1, 4, 4, null),
       (1, 6, 4, null),
       (2, 8, 4, null),
       (3, 11, 4, null);



update equipe
set idchef = 1
where nom = 'Alpha';

update equipe
set idchef = 2
where nom = 'Beta';